package com.pion.pro.eNDer.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.List;

/**
 * Created by atam on 2016. 9. 27..
 */
public class PreferenceManager {// sharedPreference 파일의 내용을 관리

    // private //
    // ==========================================
    private static PreferenceManager sInstance;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;



    private static final String PREFERENCE = "ENDER_PREFERENCE";
    private static final String PREF_ID = "USER_ID";
    private static final String PREF_PWD = "USER_PWD";
    private static final String PREF_ENDER_SEQ = "ENDER_SEQ";
    private static final String PREF_USER_SEQ = "USER_SEQ";
    private static final String PREF_USER_APP_SEQ = "USER_APP_SEQ";
    private static final String PREF_HAVE_DEVICE_SEQ = "HAVE_DEVICE_SEQ";
    private static final String PREF_COMPANY_NM = "COMPANY_NM";
    private static final String PREF_BRAND_NM = "BRAND_NM";
    private static final String PREF_PROJECT_NM = "PROJECT_NM";
    private static final String PREF_IMEI = "DEVICE_IMEI";
    private static final String PREF_START_DATE = "START_DATE";
    private static final String PREF_END_DATE = "END_DATE";
    private static final String PREF_START_DATE_STR = "START_STARTING";
    private static final String PREF_RESULT_STR = "RESULT_STR";
    private static final String PREF_PACKAGE_NM = "PACKAGE_NM";
    private static final String PREF_PID = "PREF_PID";

    //
    private static final String PREF_MEMORY_LEAK = "MEMORY_LEAK";
    private static final String PREF_BATTERY_USE = "BATTERY_USE";
    private static final String PREF_CPU_USE = "CPU_USE";
    private static final String PREF_MEMORY_H = "MEMORY_H";
    private static final String PREF_MEMORY_P = "MEMORY_P";
    private static final String PREF_MEMORY_V = "MEMORY_V";
    private static final String PREF_CAPTURE_CNT = "CAPTURE_CNT";
    private static final String PREF_EXE_DEVICE_SEQ = "EXE_DEVICE_SEQ";
    private static final String PREF_KILL = "KILL_PROCESS";




    private PreferenceManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public static synchronized PreferenceManager getInstance(Context context) { // 외부에서 사용하는 생성자, 싱글턴
        if (sInstance == null) {
            sInstance = new PreferenceManager(context);
        }
        return sInstance;
    }
    // ===========================================


    // public //
    // ==========================================

    public boolean isLogin() {
        if (TextUtils.isEmpty(mSharedPreferences.getString(PREF_ID, "")) == true ) {
            return false;
        }
        return true;
    }

    public void setLoginIdPassword(String id, String password) {
        mEditor.putString(PREF_ID, id);
        mEditor.putString(PREF_PWD, password);
        mEditor.apply();
    }

    public void clearLoginIdPassword() {
        mEditor.putString(PREF_ID, "");
        mEditor.putString(PREF_PWD, "");
        mEditor.apply();
    }

    public String getUserID() {
        return mSharedPreferences.getString(PREF_ID, "");
    }

    public String getUserPWD() {
        return mSharedPreferences.getString(PREF_PWD, "");
    }


    public void setEnderSeq(String seq) {
        mEditor.putString(PREF_ENDER_SEQ, seq);
        mEditor.apply();
    }

    public void clearEnderseq(){
        mEditor.putString(PREF_ENDER_SEQ, "");
        mEditor.apply();
    }

    public String getEnderSeq() {
        return mSharedPreferences.getString(PREF_ENDER_SEQ, "");
    }

    public void setUserSeq(String seq) {
        mEditor.putString(PREF_USER_SEQ, seq);
        mEditor.apply();
    }

    public String getUserSeq() {
        return mSharedPreferences.getString(PREF_USER_SEQ, "");
    }

    public void setUserAppSeq(String seq) {
        mEditor.putString(PREF_USER_APP_SEQ, seq);
        mEditor.apply();
    }

    public String getUserAppSeq() {
        return mSharedPreferences.getString(PREF_USER_APP_SEQ, "");
    }

    public void setHaveDeviceSeq(String seq) {
        mEditor.putString(PREF_HAVE_DEVICE_SEQ, seq);
        mEditor.apply();
    }

    public String getHaveDeviceSeq() {
        return mSharedPreferences.getString(PREF_HAVE_DEVICE_SEQ, "");
    }

    public void setCompanyNm(String seq) {
        mEditor.putString(PREF_COMPANY_NM, seq);
        mEditor.apply();
    }

    public String getCompanyNm() {
        return mSharedPreferences.getString(PREF_COMPANY_NM, "");
    }

    public void setBrandNm(String seq) {
        mEditor.putString(PREF_BRAND_NM, seq);
        mEditor.apply();
    }

    public String getBrandNm() {
        return mSharedPreferences.getString(PREF_BRAND_NM, "");
    }

    public void setProjectNm(String seq) {
        mEditor.putString(PREF_PROJECT_NM, seq);
        mEditor.apply();
    }

    public String getProjectNm() {
        return mSharedPreferences.getString(PREF_PROJECT_NM, "");
    }

    public void setImei(String imei) {
        mEditor.putString(PREF_IMEI, imei);
        mEditor.apply();
    }

    public String getImei() {
        return mSharedPreferences.getString(PREF_IMEI, "");
    }

    public void setStartDate(String strDate) {
        mEditor.putString(PREF_START_DATE, strDate);
        mEditor.apply();
    }

    public String getStartDate() {
        return mSharedPreferences.getString(PREF_START_DATE, "");
    }

    public void setEndtDate(String strDate) {
        mEditor.putString(PREF_END_DATE, strDate);
        mEditor.apply();
    }

    public String getEndDate() {
        return mSharedPreferences.getString(PREF_END_DATE, "");
    }

    public void setStartStr(String strDate) {
        mEditor.putString(PREF_START_DATE_STR, strDate);
        mEditor.apply();
    }

    public String getStartStr() {
        return mSharedPreferences.getString(PREF_START_DATE_STR, "");
    }


    public void setResultStr(String strDate) {
        mEditor.putString(PREF_RESULT_STR, strDate);
        mEditor.apply();
    }

    public String getResultStr() {
        return mSharedPreferences.getString(PREF_RESULT_STR, "");
    }

    public void setMemoryLeak(String strDate) {
        mEditor.putString(PREF_MEMORY_LEAK, strDate);
        mEditor.apply();
    }

    public String getMemoryLeak() {
        return mSharedPreferences.getString(PREF_MEMORY_LEAK, "");
    }

    public void setBatteryUse(String strDate) {
        mEditor.putString(PREF_BATTERY_USE, strDate);
        mEditor.apply();
    }

    public String getBatteryUse() {
        return mSharedPreferences.getString(PREF_BATTERY_USE, "");
    }

    public void setCPUUse(String strDate) {
        mEditor.putString(PREF_CPU_USE, strDate);
        mEditor.apply();
    }

    public String getCPUUse() {
        return mSharedPreferences.getString(PREF_CPU_USE, "");
    }

    public void setMemory_H(String strDate) {
        mEditor.putString(PREF_MEMORY_H, strDate);
        mEditor.apply();
    }

    public String getMemory_H() {
        return mSharedPreferences.getString(PREF_MEMORY_H, "");
    }

    public void setMemory_P(String strDate) {
        mEditor.putString(PREF_MEMORY_P, strDate);
        mEditor.apply();
    }

    public String getMemory_P() {
        return mSharedPreferences.getString(PREF_MEMORY_P, "");
    }

    public void setMemory_V(String strDate) {
        mEditor.putString(PREF_MEMORY_V, strDate);
        mEditor.apply();
    }

    public String getMemory_V() {
        return mSharedPreferences.getString(PREF_MEMORY_V, "");
    }

    public void setCaptureCnt(String strDate) {
        mEditor.putString(PREF_CAPTURE_CNT, strDate);
        mEditor.apply();
    }

    public String getCaptureCnt() {
        return mSharedPreferences.getString(PREF_CAPTURE_CNT, "");
    }

    public void setExeDeviceSeq(String strDate) {
        mEditor.putString(PREF_EXE_DEVICE_SEQ, strDate);
        mEditor.apply();
    }

    public String getExeDeviceSeq() {
        return mSharedPreferences.getString(PREF_EXE_DEVICE_SEQ, "");
    }

    public void setPackageNm(String packageNm) {
        mEditor.putString(PREF_PACKAGE_NM, packageNm);
        mEditor.apply();
    }

    public String getPackageNm() {
        return mSharedPreferences.getString(PREF_PACKAGE_NM, "");
    }

    public void setPid(int packageNm) {
        mEditor.putInt(PREF_PID, packageNm);
        mEditor.apply();
    }

    public int getPid() {
        return mSharedPreferences.getInt(PREF_PID, 0);
    }

//    public void setPrefKill(int kill) {
//        mEditor.putInt(PREF_KILL, kill);
//        mEditor.apply();
//    }
//
//    public int getPrefKill() {
//        return mSharedPreferences.getInt(PREF_KILL, 0);
//    }

    // ===========================================
}
