/*
Copyright (C) 2011 The University of Michigan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Please send inquiries to powertutor@umich.edu
 */

package com.pion.pro.eNDer.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.CursorLoader;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.pion.pro.eNDer.R;
import com.pion.pro.eNDer.animation.MoveAnimation;
import com.pion.pro.eNDer.animation.RemoveAnimation;
import com.pion.pro.eNDer.apiService.ServiceApiHttpClient;
import com.pion.pro.eNDer.phone.PhoneSelector;
import com.pion.pro.eNDer.ui.ReportApp;
import com.pion.pro.eNDer.ui.SelectApp;
import com.pion.pro.eNDer.util.ApiUrl;
import com.pion.pro.eNDer.util.Global;
import com.pion.pro.eNDer.util.SystemInfo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.pion.pro.eNDer.ui.UMLogger;

public class UMLoggerService extends Service implements SensorEventListener {
	private static final String TAG = "UMLoggerService";
	private static final int NOTIFICATION_ID = 1;

	Context mContext;
	private Thread estimatorThread;
	private PowerEstimator powerEstimator;
	private Thread screenshotThread;
	private OnScreenshotEventReg onScreenshotEventReg;

	private NotificationCompat.Builder notification;
	private NotificationManager notificationManager;
	private List<Integer> targetAppUidList;

	private Boolean debug = true;

	PowerManager powerManager;
	PowerManager.WakeLock wakeLock;
	WifiManager wifiManager;
	WifiLock wifiLock;
	SharedPreferences mPreferences;
//	FileOutputStream fos;
//	OutputStreamWriter osw;
//	BufferedWriter bw;
	String time;
	private boolean isGPS;
	private boolean isAudio;
	private boolean isAudioDeviceActivity;
	private boolean isCount;
	private boolean isCurrentKey;
	private boolean isLogTotal;
	private boolean isCpuUsage;
	private boolean isMemory;
	private boolean isPid;
	private boolean isCapacity;
	private boolean isTraffic;
	private boolean isWakeLock;
	private boolean isWifiLock;
	private boolean isKB;
	private boolean isRealTimeOverlay;
	private boolean isFileWrite;

	public static View oView;
	private static ImageView iView;

	//Over View Test (Floating Acttion Button, Menu
	private FloatingActionButton rightLowerButton;
	private FloatingActionMenu rightLowerMenu;
	//

	private WindowManager.LayoutParams params;
	private WindowManager wm;
	private float START_X, START_Y;
	private int PREV_X, PREV_Y;
	private int MAX_X = -1, MAX_Y = -1;
	private static int oldPid;
	private static int pidChangeCount;
	private static LayoutInflater layoutInflater;
	private static boolean isOverlayPidTime;
	UiHandler mHandler;
//	LinearLayout ll_over_view;
	ProgressBar ll_over_view;
	private boolean myTextOn = true;
	LinearLayout linearLayout;
	FloatingActionsMenu fabMenu;
//	ImageView fabIconNew;

	Button btn_serviceStart,btn_report,btn_select;

	/*
	* 자이로 스코프 센서 감지
	* */
	private long lastTime;
	private float speed;
	private float lastX;
	private float lastY;
	private float lastZ;
	private float x, y, z;
	int SHAKE_THRESHOLD = 2000;
	private static final int DATA_X = SensorManager.DATA_X;
	private static final int DATA_Y = SensorManager.DATA_Y;
	private static final int DATA_Z = SensorManager.DATA_Z;
	SensorManager sensorManager;
	Sensor accelerormeterSensor;
	Sensor oriSensor;
	Sensor cpuTempSensor;

	SensorEventListener oriSensorListen;
	SensorEventListener cpuTempSensorListen;

	private boolean statFlag = false;
	private boolean flagService = false;
	/**
	 * Service Class Var
	 * **/
//	Intent str;
	Handler slideHandler;

	private static final int LEFT_SLIDE_TRY_OPTIONS_DURATION = 1000;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		if (intent.getBooleanExtra("stop", false)) {
			this.stopSelf();
			return START_REDELIVER_INTENT;
		} else if (estimatorThread != null) {
			return START_REDELIVER_INTENT;
		}
//		else if(intent.getStringExtra("serviceIntent").equals("start")){
//			str = intent;
//		}

		showNotification();

		if (isWakeLock)
			wakeLock.acquire();
		if (isWifiLock)
			wifiLock.acquire();

		estimatorThread = new Thread(powerEstimator);
		estimatorThread.start();

		screenshotThread = new Thread(onScreenshotEventReg);
		screenshotThread.start();
//		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
//			Toast.makeText(mContext, "OS Version : " + android.os.Build.VERSION.SDK_INT, Toast.LENGTH_SHORT).show();
//		}else{
//			Toast.makeText(mContext, "OS Version : " + android.os.Build.VERSION.SDK_INT, Toast.LENGTH_SHORT).show();
//		}


		if (isRealTimeOverlay) {
//			overlayMemoryUnit = (TextView) oView.findViewById(R.id.rt_mem_unit);
		}

		return START_REDELIVER_INTENT;
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		/*
		* 자이로 스코프 센서 서비스 등록
		* */
	try{ if (accelerormeterSensor != null) sensorManager.registerListener(this, accelerormeterSensor, SensorManager.SENSOR_DELAY_GAME);}catch (Exception e){}
	}

	@Override
	public void onCreate() {

		mContext = getApplicationContext();
		mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

		isCurrentKey = mPreferences.getBoolean("pref_current_battery", true);
		isLogTotal = mPreferences.getBoolean("pref_total_battery", true);
		isGPS = mPreferences.getBoolean("pref_gps", true);
		isAudio = mPreferences.getBoolean("pref_audio", true);

		isCpuUsage = mPreferences.getBoolean("pref_cpuusage", true);
		isMemory = mPreferences.getBoolean("pref_memory", true);
		isTraffic = mPreferences.getBoolean("pref_traffic", true);

		isCount = mPreferences.getBoolean("pref_count", true);
		isAudioDeviceActivity = mPreferences.getBoolean("pref_audio_device", true);
		isPid = mPreferences.getBoolean("pref_pid", true);
		isCapacity = mPreferences.getBoolean("pref_capacity", true);

		/*
		* 자이로 스코프 센서 서비스 객체 생성
		* */
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		accelerormeterSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

		oriSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		//oriSensorListen = new oriSensorListener();
		sensorManager.registerListener(this, oriSensor, SensorManager.SENSOR_DELAY_NORMAL);    // 방향 센서 리스너 오브젝트를 등록

//		cpuTempSensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
//		if ( cpuTempSensor != null ) {
//			sensorManager.registerListener(this, cpuTempSensor, SensorManager.SENSOR_DELAY_FASTEST);    // 방향 센서 리스너 오브젝트를 등록
//		} else {
//			Log.d(TAG,"@@@@@@@@@@  Sensor.TYPE_AMBIENT_TEMPERATURE Not Found !!!!!!!! @@@@@@@@@@@@@ ");
//		}

		cpuTempSensor = sensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE);
		if ( cpuTempSensor != null ) {
			sensorManager.registerListener(this, cpuTempSensor, SensorManager.SENSOR_DELAY_NORMAL);    // 방향 센서 리스너 오브젝트를 등록
		} else {
			Log.d(TAG,"@@@@@@@@@@  Sensor.TYPE_TEMPERATURE Not Found !!!!!!!! @@@@@@@@@@@@@ ");
		}

		String powerModel = mPreferences.getString("pref_powermodel", mContext.getResources()
				.getString(R.string.default_powermodel));
		if (powerModel.equals("SAPPHIRE")) {
			PhoneSelector.PHONE_TYPE = PhoneSelector.PHONE_SAPPHIRE;
			PhoneSelector.isOLED = false;
		} else if (powerModel.equals("PASSION")) {
			PhoneSelector.PHONE_TYPE = PhoneSelector.PHONE_PASSION;
			PhoneSelector.isOLED = true;
		} else {
			PhoneSelector.PHONE_TYPE = PhoneSelector.PHONE_DREAM;
			PhoneSelector.isOLED = false;
		}
		isWakeLock = mPreferences.getBoolean("pref_wakelock", true);
		isWifiLock = mPreferences.getBoolean("pref_wifilock", true);
		isKB = mPreferences.getBoolean("pref_traffic_kb", true);
		isRealTimeOverlay = mPreferences.getBoolean("pref_overlay", true)
					& mPreferences.getBoolean("one_app", false);
		isFileWrite = mPreferences.getBoolean("pref_file_write", true);
		isOverlayPidTime = mPreferences.getBoolean("pref_overlay_pid_time", true);
		oldPid = -1;
		pidChangeCount = 0;

		powerEstimator = new PowerEstimator(this);
		onScreenshotEventReg = new OnScreenshotEventReg(this);

		/* Register to receive airplane mode and battery low messages. */
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
		filter.addAction(Intent.ACTION_BATTERY_LOW);
		filter.addAction(Intent.ACTION_BATTERY_CHANGED);
		filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		filter.addAction(Intent.ACTION_PACKAGE_REPLACED);

		notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		time = Tools.getDate();
		Log.d("Time", "time : " + time);
		powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
		wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		wifiLock = wifiManager.createWifiLock(TAG);

		if (isRealTimeOverlay) {
			////////////////////////////////////////////////////////

	        params = new WindowManager.LayoutParams(
	                WindowManager.LayoutParams.WRAP_CONTENT,
//	        		(int) getResources().getDimension(R.dimen.overlay_view),
	                WindowManager.LayoutParams.WRAP_CONTENT,
	                WindowManager.LayoutParams.TYPE_PHONE,
//	                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
//					WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN|
					WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS|
	                0 | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
	                PixelFormat.TRANSLUCENT);
	        params.gravity = Gravity.TOP | Gravity.RIGHT;

	        wm = (WindowManager) getSystemService(WINDOW_SERVICE);

	        layoutInflater = (LayoutInflater) mContext.getSystemService(
	        											Context.LAYOUT_INFLATER_SERVICE);

			addOverlayView();

	        this.mHandler = new UiHandler(this);
		}

	}

	// Handler 에서 호출하는 함수
	private void handleMessage(Message msg) {

		if (myTextOn) {
			myTextOn = false;
//			ll_over_view.setBackgroundResource(R.drawable.apptheme_scrubber_control_disabled_holo);

		} else {
			myTextOn = true;
//			ll_over_view.setBackgroundResource(R.drawable.apptheme_scrubber_control_focused_holo);
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		if (isRealTimeOverlay) {
			setMaxPosition();
			optimizePosition();
		}
	}

	private void setMaxPosition() {
		DisplayMetrics matrix = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(matrix);

		MAX_X = matrix.widthPixels - oView.getWidth();
		MAX_Y = matrix.heightPixels - oView.getHeight();
	}


	private void optimizePosition() {
		if(params.x > MAX_X) params.x = MAX_X;
		if(params.y > MAX_Y) params.y = MAX_Y;
		if(params.x < 0) params.x = 0;
		if(params.y < 0) params.y = 0;
	}

	/*
	* Overlay View FAB(Floating Action Buttong) SIZE
	* x : 420 , y : 119
	* x : 50 , y : 50
	*/
	private OnTouchListener mViewTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (MAX_X == -1) {
					setMaxPosition();
				}
				START_X = event.getRawX();
				START_Y = event.getRawY();
				PREV_X = params.x;
				PREV_Y = params.y;

				break;
			case MotionEvent.ACTION_MOVE:
				int x = (int) (START_X - event.getRawX());
				int y = (int) (event.getRawY() - START_Y);

				params.x = PREV_X + x;
				params.y = PREV_Y + y ;

				optimizePosition();
				wm.updateViewLayout(oView, params);
				break;
			}
			return true;
		}
	};

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy()");
		if (estimatorThread != null) {
			estimatorThread.interrupt();
			while (estimatorThread.isAlive()) {
				try {
					estimatorThread.join();
				} catch (InterruptedException e) {
				}
			}
		}

		//
//		if(rightLowerMenu != null && rightLowerMenu.isOpen()) rightLowerMenu.close(false);
//		if(rightLowerButton != null) rightLowerButton.detach();

		/*
		 * See comments in showNotification() for why we are using reflection
		 * here.
		 */
		boolean foregroundSet = false;
		try {
			Method stopForeground = getClass().getMethod("stopForeground", boolean.class);
			stopForeground.invoke(this, true);
			foregroundSet = true;
		} catch (InvocationTargetException e) {
		} catch (IllegalAccessException e) {
		} catch (NoSuchMethodException e) {
		}
		if (!foregroundSet) {
			stopForeground(true);
			notificationManager.cancel(NOTIFICATION_ID);
		}

//		try {
//			bw.close();
//			osw.close();
//			fos.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (NullPointerException e) {
//		}
		if (isWakeLock)
			wakeLock.release();
		if (isWifiLock)
			wifiLock.release();

		// Overlay View
		if (wm != null) {
			if(isRealTimeOverlay && (oView != null)) {
				wm.removeView(oView);
			}
        }

//		super.onDestroy();
		/*
		* 자이로 스코프 센서 서비스 해제
		* */
		if(slideHandler != null ) slideHandler.removeCallbacks(mrun);
		if (sensorManager != null) sensorManager.unregisterListener(this);

		killRunningApps(statFlag, flagService);
	}

	@Override
	public IBinder onBind(Intent intent) {

		String[] filter = powerEstimator.getComponents();

		// UTF-8 BOM for Excel CSV
		byte[] utf8Bom = { (byte)0xEF, (byte)0xBB, (byte)0xBF };
		String utf8String = new String(utf8Bom);
		StringBuilder filterStrBuilder = new StringBuilder(utf8String);


		filterStrBuilder.append(	"시간");
//		StringBuilder filterStrBuilder = new StringBuilder("시간");

		if (isCount)
			filterStrBuilder = filterStrBuilder.append(", 카운트");

		filterStrBuilder = filterStrBuilder.append(", 이름, PackageName");

		if (isCurrentKey)
			filterStrBuilder = filterStrBuilder.append(", 현재");

		if (isLogTotal)
			filterStrBuilder = filterStrBuilder.append(", 누적");

		for(int i = 0; i < filter.length - 2; i++) {
			if ((mPreferences.getInt("topIgnoreMask", 0) & 1 << i) != 0)
				continue;
			filterStrBuilder = filterStrBuilder.append(", ");
			filterStrBuilder = filterStrBuilder.append(filter[i]);
		}

		if (isCpuUsage)
			filterStrBuilder = filterStrBuilder.append(", CPU Usage");

		if (isMemory)
			filterStrBuilder = filterStrBuilder.append(", totalPSS, totalPD, totalSD, dalvikPSS, otherPSS");

		if (isCapacity)
			filterStrBuilder = filterStrBuilder.append(", Capacity");

		if (isPid)
			filterStrBuilder = filterStrBuilder.append(", PID");

		if (isTraffic) {
			if (isKB)
				filterStrBuilder = filterStrBuilder.append(", Upload(kb), Download(kb)");
			else
				filterStrBuilder = filterStrBuilder.append(", Upload(byte), Download(byte)");
		}

		if (isGPS)
			filterStrBuilder = filterStrBuilder.append(", GPS");

		if (isAudio)
			filterStrBuilder = filterStrBuilder.append(", Audio");

		if (isAudioDeviceActivity)
			filterStrBuilder = filterStrBuilder.append(", AudioActive");

		filterStrBuilder = filterStrBuilder.append("\n");
		String filterStr = filterStrBuilder.toString();

		Log.w("eNDer", filterStr);

		if (isFileWrite) {
//			try {
//				File filePath = new File(Environment.getExternalStorageDirectory() + "/Log/");
//				if(!filePath.exists())
//					filePath.mkdir();
//
//				fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/Log/",
////						"rMon" + time + ".txt"), true);
//						"eNDer" + time + ".csv"), true);
//				osw = new OutputStreamWriter(fos, "UTF-8");
//				bw = new BufferedWriter(osw);
//
//				bw.write(filterStr);
//				bw.flush();
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		}
		return binder;
	}

	public void showNotification() {
		int icon = R.drawable.appicon;

		// icon from resources
		CharSequence tickerText = "Start Monitoring"; // ticker-text
//		long when = System.currentTimeMillis(); // notification time
		Context context = getApplicationContext(); // application Context
		CharSequence contentTitle = "WODER"; // expanded message title
		CharSequence contentText = mPreferences.getString("appname", "-1/"); // expanded message text


		targetAppUidList = new ArrayList<Integer>();
		int pos = 0;
		int end;
		while ((end = contentText.toString().indexOf("/", pos)) >= 0) {
			targetAppUidList.add(Integer.parseInt(contentText.toString().substring(pos, end)));
			pos = end + 1;
		}

		if (targetAppUidList.contains(-1)) {
			contentText = "전체";
		} else {
			SystemInfo sysInfo = SystemInfo.getInstance();
			PackageManager pm = getPackageManager();

			contentText = sysInfo.getUidName(targetAppUidList.get(0), pm);
			if (targetAppUidList.size() > 1) {
				for (int i = 1; i < targetAppUidList.size(); i++)
					contentText = contentText + "/" + sysInfo.getUidName(targetAppUidList.get(i), pm);
			}
		}
//		Intent intent = context.getPackageManager().getLaunchIntentForPackage(com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).getPackageNm());
//		Intent notificationIntent = new Intent(this, UMLoggerService.class);
		Intent notificationIntent = mContext.getPackageManager().getLaunchIntentForPackage(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm());
		// the next lines initialize the Notification, using the
		// configurations above.
		notification = new NotificationCompat.Builder(context)
							.setContentTitle(contentTitle)
							.setContentText(contentText)
							.setSmallIcon(icon)
							.setTicker(tickerText)
							.setAutoCancel(false);

		//
//		RemoteView remoteView;
		//

		PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0,
														notificationIntent,
														PendingIntent.FLAG_UPDATE_CURRENT);
		notification.setContentIntent(resultPendingIntent);
		notificationManager.notify(NOTIFICATION_ID, notification.build());



		// We need to set the service to run in the foreground so that system
		// won't try to destroy the power logging service except in the most
		// critical situations (which should be fairly rare). Due to differences
		// in apis across versions of android we have to use reflection. The newer
		// api simultaneously sets an app to be in the foreground while adding a
		// notification icon so services can't 'hide' in the foreground.
		// In the new api the old call, setForeground, does nothing.
		// See: http://developer.android.com/reference/android/app/Service.html#startForeground%28int,%20android.app.Notification%29

		boolean foregroundSet = false;
		try {
			Method startForeground = getClass().getMethod("startForeground", int.class, Notification.class);
			startForeground.invoke(this, NOTIFICATION_ID, notification.build());
			foregroundSet = true;
		} catch (InvocationTargetException e) {
		} catch (IllegalAccessException e) {
		} catch (NoSuchMethodException e) {
		}
		if (!foregroundSet) {
			startForeground(NOTIFICATION_ID, notification.build());
			notificationManager.notify(NOTIFICATION_ID, notification.build());
		}
	}

	private final com.pion.pro.eNDer.service.ICounterService.Stub binder = new com.pion.pro.eNDer.service.ICounterService.Stub() {
		public String[] getComponents() {
			return powerEstimator.getComponents();
		}

		public int[] getComponentsMaxPower() {
			return powerEstimator.getComponentsMaxPower();
		}

		public int getNoUidMask() {
			return powerEstimator.getNoUidMask();
		}

		public int[] getComponentHistory(int count, int componentId, int uid) {
			return powerEstimator.getComponentHistory(count, componentId, uid, -1);
		}

		public long[] getTotals(int uid, int windowType) {
			return powerEstimator.getTotals(uid, windowType);
		}

		public long getRuntime(int uid, int windowType) {
			return powerEstimator.getRuntime(uid, windowType);
		}

		public long[] getMeans(int uid, int windowType) {
			return powerEstimator.getMeans(uid, windowType);
		}

		public byte[] getUidInfo(int windowType, int ignoreMask) {
			UidInfo[] infos = powerEstimator.getUidInfo(windowType, ignoreMask);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			try {
				new ObjectOutputStream(output).writeObject(infos);
			} catch (IOException e) {
				return null;
			}
			for (UidInfo info : infos) {
				info.recycle();
			}
			return output.toByteArray();
		}

		public long getUidExtra(String name, int uid) {
			return powerEstimator.getUidExtra(name, uid);
		}
	};

	private int count = 0;
	@Override
	public void onSensorChanged(SensorEvent event) {

		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			long currentTime = System.currentTimeMillis();
			long gabOfTime = (currentTime - lastTime);
			if (gabOfTime > 100) {
				lastTime = currentTime;
				x = event.values[SensorManager.DATA_X];
				y = event.values[SensorManager.DATA_Y];
				z = event.values[SensorManager.DATA_Z];
				speed = Math.abs(x + y + z - lastX - lastY - lastZ) / gabOfTime
						* 10000;
				if (speed > SHAKE_THRESHOLD) {

					count++;
					if(count>1 && count<=2){
						count-=1;
					}if(count>2){
						count=1;
					}
					if(ll_over_view != null)ll_over_view.setVisibility(View.GONE);
					startHandler(count);

				}
				lastX = event.values[DATA_X];
				lastY = event.values[DATA_Y];
				lastZ = event.values[DATA_Z];
			}
		} else if (event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
//			Log.i(TAG, "================ Orientation X: " + event.values[0]
//					+ ", Orientation Y: " + event.values[1]
//					+ ", Orientation Z: " + event.values[2]);

		} else if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE ) {
			Log.i(TAG, "================ TYPE_AMBIENT_TEMPERATURE : " + event.values[0]);

		} else if (event.sensor.getType() == Sensor.TYPE_TEMPERATURE ) {
			Log.i(TAG, "================ TYPE_TEMPERATURE : " + event.values[0]);

		}



	}

	private void startHandler(final int ct) {
		Handler startHandler = new Handler();

		startHandler.postDelayed(new Runnable() {
			public void run() {
				if(ct==count){
//					if (com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getCount() > Global.resource_time){
						Global.CaptureYn = true;
						Log.e(Global.LOG_TAG, "흔들감지 : CaptureYn = " + Global.CaptureYn);
						Log.e(Global.LOG_TAG, "흔들감지 : IsSingPadActivity = " + Global.IsSingPadActivity );
						count=0;
						if(ll_over_view != null) ll_over_view.setVisibility(View.VISIBLE);
//					}else{
//						Toast.makeText(UMLoggerService.this, "아직 리소스수집을 못했습니다 잠시만 기다리세요. resource Count Setting : " + Global.resource_time +"초", Toast.LENGTH_SHORT).show();
////						Handler mHandler = new Handler(Looper.getMainLooper());
////						mHandler.postDelayed(new Runnable() {
////							@Override
////							public void run() {Toast.makeText(UMLoggerService.this, "디바이스 정보 가져오지 못함", Toast.LENGTH_SHORT).show();}
////						}, 0);}
//					}
				}
			}
		}, 1000);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	public class BackgroundThread extends Thread {

		boolean running = false;

		void setRunning(boolean b) {
			running = b;
		}

		@Override
		public void run() {
			while (running) {
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				mHandler.sendMessage(mHandler.obtainMessage());
			}
		}
	}

	static class UiHandler extends Handler {

		//
		private WeakReference<UMLoggerService> umLoggerServiceWeakReference;
		public UiHandler(UMLoggerService umLoggerService){
			umLoggerServiceWeakReference = new WeakReference<UMLoggerService>(umLoggerService);
		}


		public void handleMessage(Message message) {
			super.handleMessage(message);

//			Log.d(TAG, "UiHandlerMessage : " + message.toString());
			UMLoggerService umLoggerService = umLoggerServiceWeakReference.get();
			if (umLoggerService != null) umLoggerService.handleMessage(message);

			switch (message.what) {
			case 100 :		// No Data
//				Log.d(TAG, "CPU message : " + "No Data");
//				Log.d(TAG, "Memory message : " + "No Data");
				break;
			case 110 :		// CPU On. Memory Off
//				Log.d(TAG, "CPU message : " + (CharSequence) message.obj);
				break;
			case 101 :		// CPU Off. Memory On
//				Log.d(TAG, "Memory message : " + message.arg1 / 1024.0);
				break;
			case 111 :		// CPU On. Memory On
//				Log.d(TAG, "Cpu message : " + (CharSequence) message.obj);
//				Log.d(TAG, "Memory message : " + String.format("%.2f", message.arg1 / 1024.0) +" MB");
				break;
			default :
//				Log.d(TAG, "CPU message : " + "No Data");
//				Log.d(TAG, "Memory message : " + "No Data");
			}

			// when Pid has changed
			if ((UMLoggerService.oldPid != message.arg2) && (UMLoggerService.oldPid != -1)) {
//				UMLoggerService.oView.setBackgroundColor(0xAAff0000);
//				UMLoggerService.overlayPidChangeView.setText(
//							String.format("PID 변경 %d회", ++UMLoggerService.pidChangeCount));
				Log.d(TAG, "PID 변경 : " + " "+ (++UMLoggerService.pidChangeCount));

				Log.d(TAG, "Tools.getDate() 변경 : " + " "+ (Tools.getDate()));
//				}
			} else {
//				UMLoggerService.oView.setBackgroundColor(0xAA555555);

			}
			UMLoggerService.oldPid = message.arg2;
//			Log.d(TAG, "oldPid message : " + UMLoggerService.oldPid);
		}
	}

	RelativeLayout prefLayout;
	LinearLayout tryOptionsLayout;
	private void displayOverlayContainer() {
		prefLayout = (RelativeLayout)oView.findViewById(R.id.first_layout);
		tryOptionsLayout = (LinearLayout)oView.findViewById(R.id.overlay_layout);

		tryOptionsLayout.setVisibility(View.VISIBLE);
		ll_over_view.setVisibility(View.INVISIBLE);
		int marginToSet = prefLayout.getWidth();
		tryOptionsLayout.setX(marginToSet);

		Animation animation = new MoveAnimation(prefLayout.getWidth(), tryOptionsLayout);
//		animation.setDuration(LEFT_SLIDE_TRY_OPTIONS_DURATION);
		tryOptionsLayout.startAnimation(animation);
		slideHandler = new Handler();
		slideHandler.postDelayed(mrun, 2000);
	}

	Runnable mrun = new Runnable(){
		@Override
		public void run(){
			tryOptionsLayout.setVisibility(View.INVISIBLE);
			ll_over_view.setVisibility(View.VISIBLE);
			Animation animation = new RemoveAnimation(prefLayout.getWidth(), tryOptionsLayout, (int)oView.getX());
			animation.setDuration(LEFT_SLIDE_TRY_OPTIONS_DURATION);
			tryOptionsLayout.startAnimation(animation);

			if (wm != null) {
				if(isRealTimeOverlay && (oView != null)) {
					wm.removeView(oView);
					addOverlayView();
				}
			}
		}
	};
//	MessageDialog.Builder messageDialog = new MessageDialog.Builder(UMLoggerService.this);
//	MessageDialog createDialog = null;
//	messageDialog.setTitle("Service");
//	messageDialog.setMessage("Test App Service STOP!!");
//	messageDialog.setCancelable(false);
//	createDialog = messageDialog.create();
//	createDialog.show();
//	new Handler().postDelayed(new Runnable(){
//		@Override public void run() {
//			createDialog.dismiss();
//		}
//	}, 3000);
	private void addOverlayView(){
		oView = (View) layoutInflater.inflate(R.layout.overlay_view, null);
		ll_over_view = (ProgressBar) oView.findViewById(R.id.ll_over_view);
//			serviceIntent = new Intent(this, IntroActivity.class);
		ll_over_view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				displayOverlayContainer();
			}
		});
		btn_serviceStart = (Button)oView.findViewById(R.id.btn_serviceStart);
		btn_report = (Button)oView.findViewById(R.id.btn_report);
		btn_select = (Button)oView.findViewById(R.id.btn_select);

		btn_serviceStart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				statFlag = true; flagService = true;
				UpdateAtamEnder();
				Toast.makeText(mContext, "Service Stop !!", Toast.LENGTH_LONG).show();
				try{
					if (ScreenCaptureImageService.sMediaProjection != null) {
						ScreenCaptureImageService.sMediaProjection.stop();
                        Log.d(Global.LOG_TAG, "sMidiaProjection Stop !!");
					}
					Intent intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					try {
						Thread.sleep(1000);
					}catch (Exception e){
                        e.printStackTrace();
                    }finally {
                        SelectApp.counterService = null;
                        stopService(SelectApp.serviceIntent);
//                        if(SelectApp.conn != null) getApplicationContext().unbindService(SelectApp.conn);
                        if(SelectApp.serviceIntent != null){
                            getApplicationContext().bindService(SelectApp.serviceIntent, SelectApp.conn, 0);
							onTaskRemoved(SelectApp.serviceIntent);
                        }
                    }

				}catch (Exception e){
					e.printStackTrace();
				}
//					startActivity(intent);
			}
		});
		btn_report.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				statFlag = false; flagService = false;
//				Toast.makeText(mContext, "Report Activity", Toast.LENGTH_SHORT).show();
				String enderImgsData = mPreferences.getString("enderImgsData", "");
				if ( enderImgsData.equals("") ) {
					Toast.makeText(mContext, "Image File Not Found !!! " , Toast.LENGTH_SHORT).show();
				}else{
					Intent intent = new Intent(UMLoggerService.this, ReportApp.class);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(intent);
				}


			}
		});
		btn_select.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				statFlag = true; flagService = false;
				UpdateAtamEnder();

				Handler mHandler = new Handler(Looper.getMainLooper());
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {Toast.makeText(mContext, "Wait...", Toast.LENGTH_SHORT).show();}
				}, 0);
				try {
					Thread.sleep(1000);
				}catch (Exception e){
					e.printStackTrace();
				}finally {
					Intent intent = new Intent(UMLoggerService.this, SelectApp.class);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(intent);
				}

			}
		});
		oView.setOnTouchListener(mViewTouchListener);
		//wait
		wm.addView(oView, params);
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		super.onTaskRemoved(rootIntent);
		Global.logOut(Global.LOG_TAG, "statFlag : " + statFlag + ", flagService : " + flagService +"\n rootIntent : " + rootIntent);
		killRunningApps(true, true);
	}

	private void UpdateAtamEnder() {

			final Map maps = new HashMap();

			new Thread() {
				public void run() {
					String result = "";
					try {
						// HTTP 요청 준비 작업
						ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
								"http://" + Global.getBaseUrl() + ApiUrl.API_NUM_09);

						maps.put("mirror_exe_seq", com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getExeDeviceSeq());
						maps.put("end_date", com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getEndDate());

						// 파라미터를 전송한다.
						http.addAllParameters(maps);
						//http.addOrReplace("DATA","{"+maps[0]+"}");

						// HTTP 요청 전송
						ServiceApiHttpClient post = http.create();
						post.request();

						// 응답 상태코드 가져오기
						int statusCode = post.getHttpStatusCode();

						// 응답 본문 가져오기
						String body = post.getBody();
						Log.e(Global.LOG_TAG, "ApiUrl.API_NUM_09 : " + body);
						// 실패 -> {"state":"004"}
						//여기서 부터 Json으로 파싱해서 사용해야함

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}.start();

	}

	private String getRealPathFromURI(Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };

		CursorLoader cursorLoader = new CursorLoader(this, contentUri, proj, null, null, null);
		Cursor cursor = cursorLoader.loadInBackground();

		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	void killRunningApps(boolean myPkg, boolean target)	{

		final ActivityManager activityManager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
		Log.d(TAG, "myPkg : " + myPkg + ", target : " + target);
		if (myPkg && target){
			//post delay runnable(waiting for home application launching)
			new Handler().postDelayed(new Runnable(){
				@Override public void run() {
					if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO){
						Log.i(TAG," Kill 1 "+ com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm() + " !!");
						activityManager.restartPackage(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm());
						activityManager.restartPackage(getPackageName());
					}else{
						Log.i(TAG," Kill 2 "+ com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm() + " !!" + getPackageName());
						activityManager.killBackgroundProcesses(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm());
						activityManager.killBackgroundProcesses(getPackageName());
						Log.d(TAG, "pid : " + com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPid()  + ", packageName : " + com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm());
						android.os.Process.sendSignal(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPid(), Process.SIGNAL_KILL);
						android.os.Process.sendSignal(android.os.Process.myPid(), Process.SIGNAL_KILL);
					}

					System.gc();
				}
			}, 3000);
		}else if(myPkg && !target){
			//post delay runnable(waiting for home application launching)
			new Handler().postDelayed(new Runnable(){
				@Override public void run() {
					if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO){
						Log.i(TAG," Kill 1 "+ com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm() + " !!");
						activityManager.restartPackage(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm());
					}else{
						Log.i(TAG," Kill 2 "+ com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm() + " !!" + getPackageName());
						activityManager.killBackgroundProcesses(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm());
						Log.d(TAG, "pid : " + com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPid()  + ", packageName : " + com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm());
						android.os.Process.sendSignal(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPid(), Process.SIGNAL_KILL);
					}
				}
			}, 3000);
		}else{
			return;
		}


//		Shell.SH.run("toolbox kill -9 " + com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPid());
// 		android.os.Process.killProcess(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPid());
//		android.os.Process.killProcess(android.os.Process.myPid());

	}


	//			String[] touchEvent = { "am force-stop "+com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm()+"\n",
//					"am force-stop com.pion.pro.eNDer\n",
//					};
//			try{
//				Thread.sleep(2000);
//				Process root = Runtime.getRuntime().exec(new String[] {"su"});
//				DataOutputStream os = new DataOutputStream(root.getOutputStream());
//				for(int i = 0; i < touchEvent.length; i++){
//					Log.i(TAG, touchEvent[i]);
//					os.writeBytes(touchEvent[i]);
//					os.flush();
//				}
//				root.waitFor();
//			} catch (IOException e) {
//				Log.e(TAG, "Runtime problems\n");
//				e.printStackTrace();
//			} catch (SecurityException se){
//				se.printStackTrace();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}


}