package com.pion.pro.eNDer.service;

/**
 * Created by joy on 2016. 9. 20..
 */

import android.net.Uri;
import android.os.Environment;
import android.os.FileObserver;
import android.util.Log;

import java.io.File;

public class ScreenshotObserver extends FileObserver {
    private static final String TAG = "ScreenshotObserver";
    private static final String PATH = Environment.getExternalStorageDirectory().toString() + "/Pictures/Screenshots/";

    private OnScreenshotTakenListener mListener;
    private String mLastTakenPath;

    public ScreenshotObserver(OnScreenshotTakenListener listener) {
        super(PATH, FileObserver.CLOSE_WRITE);
        mListener = listener;
    }

    @Override
    public void onEvent(int event, String path) {
        Log.i(TAG, "Event:"+event+"\t"+PATH+path);
//Screenshot_2016-11-24-13-04-33, Screenshot_2016-11-24-13-06-17,
        if (path==null || event!=FileObserver.CLOSE_WRITE) {
            Log.i(TAG, "Don't care.");
            path = mLastTakenPath;
            //start();
        }
        else if (mLastTakenPath!=null && path.equalsIgnoreCase(mLastTakenPath)) {
            Log.i(TAG, "This event has been observed before.");
        }
        else {
            mLastTakenPath = path;
            File file = new File(PATH+path);
            
            mListener.onScreenshotTaken(Uri.fromFile(file));
            Log.i(TAG, "Send event to listener.");
        }
    }

    public void start() {
        Log.i(TAG,"================= ScreenshotObserver Start ======================");
        super.startWatching();
    }

    public void stop() {
        super.stopWatching();
    }



//	private Uri getLastScreenshotUri() {
//		File[] files = mScreenshotFolder.listFiles(new FileFilter() {
//			public boolean accept(File file) {
//				return file.isFile();
//			}
//		});
//		long modifiedTime = Long.MIN_VALUE;
//		File lastScreenshot = null;
//		for (File file : files) {
//			if (file.lastModified() > modifiedTime) {
//				lastScreenshot = file;
//				modifiedTime = file.lastModified();
//			}
//		}
//
//		return Uri.fromFile(lastScreenshot);
//	}
}