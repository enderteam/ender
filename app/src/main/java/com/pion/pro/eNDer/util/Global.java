package com.pion.pro.eNDer.util;

import android.os.Environment;
import android.util.Log;

/**
 * Created by JungjoongKim on 2016. 9. 21..
 *
 */
public class Global {
    /**
     * 로그 출력 시 보여지는 TAG
     */
    public static final String LOG_TAG = "WONDER";

    /**
     * 편집 이미지 저장 위치
     */
    public static final String Wonder_IMG_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Wonder";


    /*
    * app Package Name
    * */

    public static final String PACKAGE_NAME ="";    //<-- 패키지명 바꾸면 지정

    /*
    * app Activity Instance
    * */

    //public static Activity ACTIVITY_INSTANCE;
    public static Boolean CaptureYn = false;

    // check top activity signpad
    public static Boolean IsSingPadActivity = false;

//    public static int processKill = 0;
    /*
    * 로그 출력 여부
    * */
    public static final boolean DEFAULT_LOGGING_MODE = true;
//  public static final boolean DEFAULT_LOGGING_MODE = false;

    public static void logOut(String className, String Msg) {
        if ( DEFAULT_LOGGING_MODE ) Log.i(className,Msg);
    }

    public static void logOut(String Msg) {
        if ( DEFAULT_LOGGING_MODE ) Log.i("WONDER_LOG",Msg);
    }

    /**
     * 리소스 수집 Default Time
    * */
    public static int resource_time = 1000;//

    /**
     * 장면에 대한 자동 스크린 캡쳐 Default Time
     * */
    public static int scene_time = 50000;

    /*
    * 운영 및 개발 서버 URL 설정
    * */
    public static final boolean REAL_IP = true;
//    public static final boolean REAL_IP = false;

    public static String getFileServerUrl(){
        if (REAL_IP){
            return "218.38.54.177:9002";       //실 운영할 서버 주소 설정
//            return "192.168.1.56:9002";     //test(ATAM Test PC KB국민카드 지원)
        }else {
            return "218.38.54.177:9002";
        }
    }
    public static String getBaseUrl(){
        if (REAL_IP){
//            return "192.168.1.3:8080";  //실 운영할 서버 주소 설정
//            return "192.168.1.56:8080";  //test(ATAM Test PC KB국민카드 지원)
            return "106.245.92.237:8080";  //실 운영할 서버 주소 설정(192.168.1.3:8080 포워딩 ip 주소)

        }else{
//            return "192.168.0.2:8080";
//            return "192.168.0.191:8080";
//            return "10.104.196.108:8080";
//            return "192.168.0.21:8080";  //
//            return "10.104.196.85:8080";      //
//            return "192.168.0.13:8080";      //Jungjoong.Kim Home ip
//            return "192.168.0.6:8080";      //cafe(5G)
            return "192.168.1.19:8080";  //JungJoong.Kim WindowPC
        }
    }

}
