package com.pion.pro.eNDer.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pion.pro.eNDer.R;
import com.pion.pro.eNDer.apiService.ServiceApiHttpClient;
import com.pion.pro.eNDer.ui.view.AnimActivity;
import com.pion.pro.eNDer.util.ApiUrl;
import com.pion.pro.eNDer.util.Global;
import com.pion.pro.eNDer.util.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AnimActivity implements LoaderCallbacks<Cursor> {


    private  String TAG = "LoginActivity";
    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mIdView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    protected Context mContext;
    private ImageView id_icon;
    private ImageView ps_icon;

    //
    String imei;        //디바이스 고유번호
    String device_name;    //디바이스명
    String cd_name;        //제조사명
    int width;            //x 축
    int height;            //y 축
    String os_ver;        //OS Version
    String device_id;   //device id
    String device_seq;  //device seq
    String have_device_seq; //
    String ip_addr;
    String port_no;
    String state;       //api state
//    Typeface type;

    public static LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_login);
//        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

        mContext = this;


        if (SelectApp.conn != null) stopService(SelectApp.serviceIntent);
        if (PreferenceManager.getInstance(mContext) != null) {
            UpdateAtamEnderIsNull();
        }
        //get Deivice IMEI NUMBER
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        final String tmDevice, tmSerial, androidId;
//
//        tmDevice = "" + telephonyManager.getDeviceId();
//        tmSerial = "" + telephonyManager.getSimSerialNumber();
//        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
//
//        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
//
//        String deviceId = deviceUuid.toString();
//
//        String deviceID = android.provider.Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        try {
            imei = (String) Build.class.getField("SERIAL").get(null);
        } catch (Exception e) {
        }

        Log.d(Global.LOG_TAG, "imei : " + imei + ", LogingetIntent Extra String NAME : " + Build.MODEL + ", " + Build.VERSION.RELEASE.substring(0, 3).toString());

        device_name = Build.MODEL;
        cd_name = Build.MANUFACTURER;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        os_ver = Build.VERSION.RELEASE.substring(0, 3).toString();

        //공유 변수에 저장 (로그데이터에 쌓을 목적으로 저장함)
        com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setImei(imei);

//        type = Typeface.createFromAsset(this.getAssets(), "NotoSansCJKkr-Regular.otf");


        id_icon = (ImageView) findViewById(R.id.id_icon);
        ps_icon = (ImageView) findViewById(R.id.ps_icon);
        // Set up the login form.
        mIdView = (AutoCompleteTextView) findViewById(R.id.id);
        setFontToView(mIdView);
        mIdView.setText("admin");   //a1111
//        mIdView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus){
//                    id_icon.setImageResource(R.drawable.mail_icon_01);
//                    ps_icon.setImageResource(R.drawable.ps_icon_00);
//                }else{
//                    ps_icon.setImageResource(R.drawable.ps_icon_01);
//                    id_icon.setImageResource(R.drawable.mail_icon_00);
//                }
//            }
//        });
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        setFontToView(mPasswordView);
        mPasswordView.setText("admin"); //a1111
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }

                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        setFontToView(mEmailSignInButton);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mEmailSignInButton.setClickable(true);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);


        // startLocationService
        //startLocationService();


        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // GPS 프로바이더 사용가능여부
        Boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // 네트워크 프로바이더 사용가능여부
        Boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Global.logOut(TAG, "======== isGPSEnabled="+ isGPSEnabled);
        Global.logOut(TAG, "======== isNetworkEnabled="+ isNetworkEnabled);

        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                double lat = location.getLatitude();
                double lng = location.getLongitude();

                Global.logOut("===================================== latitude: "+ lat +", longitude: "+ lng);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                Global.logOut("onStatusChanged");
            }

            public void onProviderEnabled(String provider) {
                Global.logOut("onProviderEnabled");
            }

            public void onProviderDisabled(String provider) {
                Global.logOut("onProviderDisabled");
            }
        };

        // 최소 GPS 정보 업데이트 시간 밀리세컨이므로 1분 : 서버에서 가져온 시간으로 할당
        //final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
        //final long MIN_TIME_BW_UPDATES = 1000 * 5; // 5초에 한번씩

        // 최소 GPS 정보 업데이트 거리 10미터
        //final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;

        // Register the listener with the Location Manager to receive location updates
//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);

        // 수동으로 위치 구하기
//        String locationProvider = LocationManager.GPS_PROVIDER;
//        //String locationProvider = LocationManager.NETWORK_PROVIDER;
//        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
//        if (lastKnownLocation != null) {
//            double lng = lastKnownLocation.getLatitude();
//            double lat = lastKnownLocation.getLatitude();
//            Global.logOut(TAG, "==== longtitude=" + lng + ", latitude=" + lat);
//        }



    }

//    private void startLocationService() {
//
//        // get manager instance
//        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
//        // set listener
//        GPSListener gpsListener = new GPSListener();
//        long minTime = 10000;
//        float minDistance = 0;
//
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        manager.requestLocationUpdates(
//                LocationManager.GPS_PROVIDER,
//                minTime,
//                minDistance,
//                gpsListener);
//
//        Toast.makeText(getApplicationContext(), "Location Service started.\nyou can test using DDMS.", Toast.LENGTH_LONG).show();
//    }

//    private class GPSListener implements LocationListener {
//
//        public void onLocationChanged(Location location) {
//            //capture location data sent by current provider
//            Double latitude = location.getLatitude();
//            Double longitude = location.getLongitude();
//
//            String msg = "Latitude : "+ latitude + "\nLongitude:"+ longitude;
//            Log.i("================ GPS ", msg);
//            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
//
//        }
//
//        public void onProviderDisabled(String provider) {
//        }
//
//        public void onProviderEnabled(String provider) {
//        }
//
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//        }
//
//    }

    public void setFontToView(TextView... views) {
//        for (TextView view : views)
//            view.setTypeface(type);
    }


    @Override
    protected void onResume() {
        super.onResume();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {        //수정
            return;
        }


        // Reset errors.
        mIdView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String id = mIdView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(id)) {
            mIdView.setError(getString(R.string.error_field_required));
            focusView = mIdView;
            cancel = true;
        } else if (!isEmailValid(id)) {
            mIdView.setError(getString(R.string.error_invalid_email));
            focusView = mIdView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            //keyboard hide event
            // Check if no view has focus:
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            showProgress(true);
            mAuthTask = new UserLoginTask(id, password);
            //Map Param -> AsyncTask put id, password
            HashMap params = new HashMap();
//            Json --> 바꾸기
            try{
                JSONObject jsonObject = new JSONObject();

                params.put("state", "request");
                params.put("code", "201");
                params.put("MEMBER_ID", id);
                params.put("MEMBER_PASSWORD", password);
                mAuthTask.execute(params);
            }catch (Exception e){e.printStackTrace();}


        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 4;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // super.onBackPressed(); //지워야 실행됨

        AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setMessage("WONDER Exit?");
        d.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // process전체 종료
//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.addCategory(Intent.CATEGORY_HOME);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
                ((Activity)mContext).finishAndRemoveTask();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
//				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                ActivityCompat.finishAffinity(LoginActivity.this);
                System.runFinalizersOnExit(true);
                moveTaskToBack(true);
                finish();

            }
        });
        d.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        d.show();
    }

    private void UpdateAtamEnderIsNull() {

        final Map maps = new HashMap();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String result = "";
                try {
                    // HTTP 요청 준비 작업
                    ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
                            "http://" + Global.getBaseUrl() + ApiUrl.API_NUM_10);

                    maps.put("user_seq", PreferenceManager.getInstance(mContext).getUserSeq());
                    maps.put("have_device_seq", PreferenceManager.getInstance(mContext).getHaveDeviceSeq());
                    maps.put("end_date", PreferenceManager.getInstance(mContext).getEndDate());
                    // 파라미터를 전송한다.
                    http.addAllParameters(maps);
                    //http.addOrReplace("DATA","{"+maps[0]+"}");

                    // HTTP 요청 전송
                    ServiceApiHttpClient post = http.create();
                    post.request();

                    // 응답 상태코드 가져오기
                    int statusCode = post.getHttpStatusCode();

                    // 응답 본문 가져오기
                    String body = post.getBody();
                    Global.logOut("ApiUrl.API_NUM_10 : " + body);
                    // 실패 -> {"state":"004"}
                    //여기서 부터 Json으로 파싱해서 사용해야함

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

        /*new Thread() {
            public void run() {
                String result = "";
                try {
                    // HTTP 요청 준비 작업
                    ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
                            "http://" + Global.getBaseUrl() + ApiUrl.API_NUM_10);

                    maps.put("user_seq", PreferenceManager.getInstance(mContext).getUserSeq());
                    maps.put("have_device_seq", PreferenceManager.getInstance(mContext).getHaveDeviceSeq());
                    maps.put("end_date", PreferenceManager.getInstance(mContext).getEndDate());
                    // 파라미터를 전송한다.
                    http.addAllParameters(maps);
                    //http.addOrReplace("DATA","{"+maps[0]+"}");

                    // HTTP 요청 전송
                    ServiceApiHttpClient post = http.create();
                    post.request();

                    // 응답 상태코드 가져오기
                    int statusCode = post.getHttpStatusCode();

                    // 응답 본문 가져오기
                    String body = post.getBody();
                    Log.e(Global.LOG_TAG, "ApiUrl.API_NUM_10 : " + body);
                    // 실패 -> {"state":"004"}
                    //여기서 부터 Json으로 파싱해서 사용해야함

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();*/

    }

    private void deviceJoinEnderAPI(){

        final Map maps = new HashMap();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // HTTP 요청 준비 작업
                    ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
                            "http://" + Global.getBaseUrl() + ApiUrl.API_NUM_03);
                    Log.e(Global.LOG_TAG, "ApiUrl.imei : " + cd_name.toUpperCase().toString() + "," +imei + ", " + device_name + ", " + String.valueOf(width) +", " + String.valueOf(height) +", " + os_ver);
                    maps.put("imei", imei);
                    maps.put("device_nm", device_name);
                    maps.put("cd_desc", cd_name.toUpperCase().toString());
                    maps.put("size_x", String.valueOf(width));
                    maps.put("size_y", String.valueOf(height));
                    maps.put("os_ver", os_ver);
                    maps.put("user_seq", PreferenceManager.getInstance(mContext).getUserSeq());

                    // 파라미터를 전송한다.
                    http.addAllParameters(maps);
                    //http.addOrReplace("DATA","{"+maps[0]+"}");

                    // HTTP 요청 전송
                    ServiceApiHttpClient post = http.create();
                    post.request();

                    // 응답 상태코드 가져오기
                    int statusCode = post.getHttpStatusCode();

                    // 응답 본문 가져오기
                    String body = post.getBody();
                    Global.logOut("ApiUrl.API_NUM_03 : " + body);
                    // 실패 -> {"state":"004"}
                    //여기서 부터 Json으로 파싱해서 사용해야함

                    JSONObject jsonObject;
                    try{
                        if (!body.equals("") || !body.isEmpty()){
                            jsonObject = new JSONObject(body);
                            if(jsonObject != null && !jsonObject.equals("")){
                                device_id = jsonObject.getString("device_id") != null ? jsonObject.getString("device_id") : "";
                                device_seq = jsonObject.getString("device_seq") != null ? jsonObject.getString("device_seq") : "";
                                have_device_seq = jsonObject.getString("have_device_seq") != null ? jsonObject.getString("have_device_seq") : "";
                                com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setHaveDeviceSeq(have_device_seq);
                                imei = jsonObject.getString("imei") != null ? jsonObject.getString("imei") : "";
                                //ip_addr = jsonObject.getString("ip_addr") != null ? jsonObject.getString("ip_addr") : "";
                                //port_no = jsonObject.getString("port_no") != null ? jsonObject.getString("port_no") : "";
                                os_ver = jsonObject.getString("os_ver") != null ? jsonObject.getString("os_ver") : "";
                                state = jsonObject.getString("state") != null ? jsonObject.getString("state") : "";
                                if(state.equals("003")){
                                    Log.d(TAG,"등록되지 않는 제조사 디바이스 입니다. 아탐 서버에 제조사를 등록바랍니다.");
//                                    Handler mHandler = new Handler(Looper.getMainLooper());
//                                    mHandler.postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Toast.makeText(LoginActivity.this, "등록되지 않는 제조사 디바이스 입니다. 아탐 서버에 제조사를 등록바랍니다.", Toast.LENGTH_SHORT).show();
//                                        }
//                                    }, 0);
                                }else if(state.equals("004")){
//                                    Handler mHandler = new Handler(Looper.getMainLooper());
//                                    mHandler.postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Toast.makeText(LoginActivity.this, "등록할 수 없는 디바이스 입니다. 관리자에게 문의하여 주시기 바랍니다.", Toast.LENGTH_SHORT).show();
//                                        }
//                                    }, 0);
                                }else if(state.equals("001")){
//                                    Handler mHandler = new Handler(Looper.getMainLooper());
//                                    mHandler.postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {Toast.makeText(LoginActivity.this, "등록 확인되었습니다.", Toast.LENGTH_SHORT).show();}
//                                    }, 0);
                                }else if(state.equals("101")){
//                                    Handler mHandler = new Handler(Looper.getMainLooper());
//                                    mHandler.postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {Toast.makeText(LoginActivity.this, "등록 되었습니다.", Toast.LENGTH_SHORT).show();}
//                                    }, 0);
                                }
                            } else {
//                                Handler mHandler = new Handler(Looper.getMainLooper());
//                                mHandler.postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {Toast.makeText(LoginActivity.this, "디바이스 정보 가져오지 못함", Toast.LENGTH_SHORT).show();}
//                                }, 0);
                            }
                        }else{
//                            Handler mHandler = new Handler(Looper.getMainLooper());
//                            mHandler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {Toast.makeText(LoginActivity.this, "디바이스 정보 가져오지 못함", Toast.LENGTH_SHORT).show();}
//                            }, 0);
                        }

                    }catch (JSONException e){e.printStackTrace();}

                } catch (Exception e) {e.printStackTrace();}
            }
        });
        thread.setDaemon(true);
        thread.start();

        /*new Thread() {
            public void run() {
                try {
                    // HTTP 요청 준비 작업
                    ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
                            "http://" + Global.getBaseUrl() + ApiUrl.API_NUM_03);
                    Log.e(Global.LOG_TAG, "ApiUrl.imei : " + cd_name.toUpperCase().toString() + "," +imei + ", " + device_name + ", " + String.valueOf(width) +", " + String.valueOf(height) +", " + os_ver);
                    maps.put("imei", imei);
                    maps.put("device_nm", device_name);
                    maps.put("cd_desc", cd_name.toUpperCase().toString());
                    maps.put("size_x", String.valueOf(width));
                    maps.put("size_y", String.valueOf(height));
                    maps.put("os_ver", os_ver);
                    maps.put("user_seq", PreferenceManager.getInstance(mContext).getUserSeq());

                    // 파라미터를 전송한다.
                    http.addAllParameters(maps);
                    //http.addOrReplace("DATA","{"+maps[0]+"}");

                    // HTTP 요청 전송
                    ServiceApiHttpClient post = http.create();
                    post.request();

                    // 응답 상태코드 가져오기
                    int statusCode = post.getHttpStatusCode();

                    // 응답 본문 가져오기
                    String body = post.getBody();
                    Log.e(Global.LOG_TAG, "ApiUrl.API_NUM_03 : " + body);
                    // 실패 -> {"state":"004"}
                    //여기서 부터 Json으로 파싱해서 사용해야함

                    JSONObject jsonObject;
                    try{
                        if (!body.equals("") || !body.isEmpty()){
                            jsonObject = new JSONObject(body);
                            if(jsonObject != null && !jsonObject.equals("")){
                                device_id = jsonObject.getString("device_id") != null ? jsonObject.getString("device_id") : "";
                                device_seq = jsonObject.getString("device_seq") != null ? jsonObject.getString("device_seq") : "";
                                have_device_seq = jsonObject.getString("have_device_seq") != null ? jsonObject.getString("have_device_seq") : "";
                                com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setHaveDeviceSeq(have_device_seq);
                                imei = jsonObject.getString("imei") != null ? jsonObject.getString("imei") : "";
                                //ip_addr = jsonObject.getString("ip_addr") != null ? jsonObject.getString("ip_addr") : "";
                                //port_no = jsonObject.getString("port_no") != null ? jsonObject.getString("port_no") : "";
                                os_ver = jsonObject.getString("os_ver") != null ? jsonObject.getString("os_ver") : "";
                                state = jsonObject.getString("state") != null ? jsonObject.getString("state") : "";
                                if(state.equals("003")){
                                    Log.d(TAG,"등록되지 않는 제조사 디바이스 입니다. 아탐 서버에 제조사를 등록바랍니다.");
//                                    Handler mHandler = new Handler(Looper.getMainLooper());
//                                    mHandler.postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Toast.makeText(LoginActivity.this, "등록되지 않는 제조사 디바이스 입니다. 아탐 서버에 제조사를 등록바랍니다.", Toast.LENGTH_SHORT).show();
//                                        }
//                                    }, 0);
                                }else if(state.equals("004")){
//                                    Handler mHandler = new Handler(Looper.getMainLooper());
//                                    mHandler.postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Toast.makeText(LoginActivity.this, "등록할 수 없는 디바이스 입니다. 관리자에게 문의하여 주시기 바랍니다.", Toast.LENGTH_SHORT).show();
//                                        }
//                                    }, 0);
                                }else if(state.equals("001")){
//                                    Handler mHandler = new Handler(Looper.getMainLooper());
//                                    mHandler.postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {Toast.makeText(LoginActivity.this, "등록 확인되었습니다.", Toast.LENGTH_SHORT).show();}
//                                    }, 0);
                                }else if(state.equals("101")){
//                                    Handler mHandler = new Handler(Looper.getMainLooper());
//                                    mHandler.postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {Toast.makeText(LoginActivity.this, "등록 되었습니다.", Toast.LENGTH_SHORT).show();}
//                                    }, 0);
                                }
                            } else {
//                                Handler mHandler = new Handler(Looper.getMainLooper());
//                                mHandler.postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {Toast.makeText(LoginActivity.this, "디바이스 정보 가져오지 못함", Toast.LENGTH_SHORT).show();}
//                                }, 0);
                            }
                        }else{
//                            Handler mHandler = new Handler(Looper.getMainLooper());
//                            mHandler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {Toast.makeText(LoginActivity.this, "디바이스 정보 가져오지 못함", Toast.LENGTH_SHORT).show();}
//                            }, 0);
                        }

                    }catch (JSONException e){e.printStackTrace();}

                } catch (Exception e) {e.printStackTrace();}
            }
        }.start();*/


    }
    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mIdView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Map, Integer, String> {

        private final String mId;
        private final String mPassword;



        UserLoginTask(String id, String password) {
            mId= id;
            mPassword = password;
        }

        @Override
        protected String doInBackground(Map... maps) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return null;
            }
            // HTTP 요청 준비 작업
            ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
                    "http://"+ Global.getBaseUrl()+ ApiUrl.API_NUM_02);

            // 파라미터를 전송한다.
            http.addAllParameters(maps[0]);
//            http.addOrReplace("DATA","{"+maps[0]+"}");

            // HTTP 요청 전송
            ServiceApiHttpClient post = http.create();
            post.request();

            // 응답 상태코드 가져오기
            int statusCode = post.getHttpStatusCode();

            // 응답 본문 가져오기
            String body = post.getBody();

            return body;
            // TODO: register the new account here.
        }

        @Override
        protected void onPostExecute(String s) {
            Global.logOut("ApiUrl.API_NUM_02 : \n" + s);
            mAuthTask = null;
            showProgress(false);
            JSONObject jsonObject;
            try{
                if (!s.equals("") || !s.isEmpty()){
                    jsonObject = new JSONObject(s);
                    if(jsonObject != null && !jsonObject.equals("")){
                        Log.d("UserLoginTask", "user_name : " + jsonObject.getString("user_name") + ", user_seq : " +jsonObject.getString("user_seq")+", user_pwd : " +jsonObject.getString("user_pwd"));
                        com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setUserSeq(jsonObject.getString("user_seq"));
//                        Intent mIntent = new Intent(LoginActivity.this, UMLogger.class);
                        Intent mIntent = new Intent(LoginActivity.this, SelectApp.class);
                        mIntent.putExtra("user_name",jsonObject.getString("user_name"));
                        mIntent.putExtra("user_seq",jsonObject.getString("user_seq"));
                        mIntent.putExtra("user_pwd",jsonObject.getString("user_pwd"));
                        PreferenceManager.getInstance(mContext).clearLoginIdPassword();
                        PreferenceManager.getInstance(mContext).setLoginIdPassword(jsonObject.getString("user_name"),jsonObject.getString("user_pwd"));
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        if (jsonObject.getString("user_seq") != null){
                            deviceJoinEnderAPI();
                            // Simulate network access delay.
                            try {Thread.sleep(2000);} catch (InterruptedException e) {e.printStackTrace();}
                            startActivity(mIntent);
                            finish();
                        }
                    }else{
                        Toast.makeText(LoginActivity.this, "This id and password is invalid", Toast.LENGTH_SHORT).show();
                        mPasswordView.setError(getString(R.string.error_incorrect_password));
                        mPasswordView.requestFocus();
                        mPasswordView.setText("");
                    }
                }else{
                    Toast.makeText(LoginActivity.this, "This id and password is invalid", Toast.LENGTH_SHORT).show();
                    mIdView.setError(getString(R.string.error_invalid_email));
                    mIdView.requestFocus();
                    mPasswordView.setText("");
                }

            }catch (Exception e){
                e.printStackTrace();
            }

        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);

        }
    }

//    @Override
//    protected void onDestroy() {
//        moveTaskToBack(true);
//        finish();
//    }
}

