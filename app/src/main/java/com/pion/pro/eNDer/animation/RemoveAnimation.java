package com.pion.pro.eNDer.animation;

/**
 * Created by atam on 2016. 10. 18..
 */
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

public class RemoveAnimation extends Animation {
    private final int margin, currentMargin;
    private final LinearLayout layoutToRemove;

    public RemoveAnimation(int margin, LinearLayout layoutToRemove, int currentMargin) {
        this.margin = margin;
        this.layoutToRemove = layoutToRemove;
        this.currentMargin = currentMargin;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        int marginToSet = currentMargin + (int) ((margin - currentMargin) * interpolatedTime);
        layoutToRemove.setX(marginToSet);
    }
}