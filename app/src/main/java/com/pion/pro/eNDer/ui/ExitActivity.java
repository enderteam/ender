package com.pion.pro.eNDer.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pion.pro.eNDer.R;
import com.pion.pro.eNDer.apiService.PackageInfo;
import com.pion.pro.eNDer.apiService.ProjectListVO;
import com.pion.pro.eNDer.apiService.ServiceApiHttpClient;
import com.pion.pro.eNDer.service.ICounterService;
import com.pion.pro.eNDer.service.ScreenCaptureImageService;
import com.pion.pro.eNDer.service.UMLoggerService;
import com.pion.pro.eNDer.ui.view.AnimActivity;
import com.pion.pro.eNDer.ui.view.ListDialog;
import com.pion.pro.eNDer.util.ApiUrl;
import com.pion.pro.eNDer.util.Global;
import com.pion.pro.eNDer.util.SystemInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExitActivity extends AnimActivity {

	private SharedPreferences mPreferences;

	ProgressBar progressBar;
	Context context;
	Intent intent ;

	public static Intent serviceIntent;
	public static ICounterService counterService;
	public static CounterServiceConnection conn;

	Handler overlayHandler;
	ProgressDialog progressDialog;
	Handler pHander;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.selectapp);
//		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);
		Global.IsSingPadActivity = true;
//		UmLoggerOverlayViewVisivility(1);


		context = ExitActivity.this;
		intent = getIntent();
		mPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		UpdateAtamEnder();
		serviceIntent = new Intent(this, UMLoggerService.class);
		conn = new CounterServiceConnection();
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			if (ScreenCaptureImageService.sMediaProjection != null) {
				ScreenCaptureImageService.sMediaProjection.stop();
				Global.logOut(Global.LOG_TAG, "sMidiaProjection Stop !!");
			}
		}else{
			Global.logOut(Global.LOG_TAG, "sMidiaProjection not Stop !!");
		}
		pHander = new Handler();
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				progressDialog = new ProgressDialog(ExitActivity.this);
//		progressDialog.setTitle("잠시만 기다려주세요");
//		progressDialog.setMessage("설치된 어플리케이션 목록을 확인중..");
				progressDialog.setCanceledOnTouchOutside(false);
				progressDialog.show();
				progressDialog.setContentView(R.layout.view_progressdialog_exit);
				progressDialog.setCancelable(true);
//				progressDialog.setMax(5000 - 1);
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		Global.CaptureYn = false;
//		UmLoggerOverlayViewVisivility(1);
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				getApplicationContext().bindService(serviceIntent, conn, 0);
			}
		}, 250);
	}

	@Override
	protected void onStop() {
		super.onStop();
		Global.IsSingPadActivity = false;
//		UmLoggerOverlayViewVisivility(0);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (conn != null){
			getApplicationContext().unbindService(conn);
		}
		Global.IsSingPadActivity = false;
//		UmLoggerOverlayViewVisivility(0);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Global.IsSingPadActivity = false;
//		UmLoggerOverlayViewVisivility(0);
	}

	@Override
	protected void onUserLeaveHint() {
		super.onUserLeaveHint();
		Global.logOut(ExitActivity.class.getSimpleName(), "Home Button Touch");
//		UmLoggerOverlayViewVisivility(1);
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		Global.logOut(ExitActivity.class.getSimpleName(), "onUserInteraction()");
	}

	private void closeActivity(){

		pHander.postDelayed(new Runnable() {
			@Override
			public void run() {

				/*// only API 16+
				progressDialog.dismiss();
				((Activity)context).finishAffinity();
				// support library v4
				ActivityCompat.finishAffinity(((Activity)context));*/
				if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {((Activity)context).finishAndRemoveTask();}
				else{((Activity)context).finishAffinity();}

				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
//				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				progressDialog.dismiss();
				startActivity(intent);
				ActivityCompat.finishAffinity(ExitActivity.this);
				System.runFinalizersOnExit(true);
				moveTaskToBack(true);
				finish();
			}
		}, 3000);

	}

	private void UpdateAtamEnder() {
		final Map maps = new HashMap();

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				String result = "";
				try {
					// HTTP 요청 준비 작업
					ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
							"http://" + Global.getBaseUrl() + ApiUrl.API_NUM_09);

					maps.put("mirror_exe_seq", com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).getExeDeviceSeq());
					maps.put("end_date", com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).getEndDate());

					// 파라미터를 전송한다.
					http.addAllParameters(maps);
					//http.addOrReplace("DATA","{"+maps[0]+"}");

					// HTTP 요청 전송
					ServiceApiHttpClient post = http.create();
					post.request();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		thread.setDaemon(true);
		thread.start();

	}

	public class CounterServiceConnection implements ServiceConnection {
		public void onServiceConnected(ComponentName className, IBinder boundService) {
			counterService = ICounterService.Stub.asInterface((IBinder) boundService);
//			UmLoggerOverlayViewVisivility(2);
			counterService = null;
			stopService(serviceIntent);
			getApplicationContext().unbindService(conn);
			getApplicationContext().bindService(serviceIntent, conn, 0);

			Log.e(ExitActivity.class.getSimpleName(), "Service Stop Call!!!!");
			closeActivity();
//			Thread thread = new Thread(new Runnable() {
//				@Override
//				public void run() {
//
//				}
//			});
//			thread.setDaemon(true);
//			thread.start();

		}

		public void onServiceDisconnected(ComponentName className) {
			counterService = null;
			getApplicationContext().unbindService(conn);
			getApplicationContext().bindService(serviceIntent, conn, 0);
			try{
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						progressDialog.dismiss();
						Intent intent = new Intent(ExitActivity.this, IntroActivity.class);
						intent.addCategory(Intent.CATEGORY_HOME);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(intent);
					}
				});
				thread.setDaemon(true);
				thread.start();
			}catch (Exception e){

			}
		}
	}



}