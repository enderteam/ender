package com.pion.pro.eNDer.animation;

/**
 * Created by atam on 2016. 10. 18..
 */

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/**
 * @author Yogesh C. Jadhav
 * @since 1.0
 */
public class MoveAnimation extends Animation {
    private final int margin;
    private final LinearLayout layoutToMove;

    public MoveAnimation(int margin, LinearLayout layoutToMove) {
        this.margin = margin;
        this.layoutToMove = layoutToMove;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        int layoutXToSet = (int) (margin - (margin * interpolatedTime));
        layoutToMove.setX(layoutXToSet);
    }
}