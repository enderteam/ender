package com.pion.pro.eNDer.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joy on 2016. 11. 18..
 */

public class EnderImgsData {

    private List<ImageData> enderImageData;

    public List<ImageData> getEnderImageData() {
        if ( enderImageData == null ) enderImageData = new ArrayList<ImageData>();
        return enderImageData;
    }

    public void setEnderImageData(List<ImageData> enderImageData) {
        this.enderImageData = enderImageData;
    }

    public static class ImageData {
        private String brand_nm;
        private String uriFileName;
        private String editPop;
        private String resStr;

        public String getBrand_nm() {
            return brand_nm;
        }

        public void setBrand_nm(String brand_nm) {
            this.brand_nm = brand_nm;
        }

        public String getUriFileName() {
            return uriFileName;
        }

        public void setUriFileName(String uriFileName) {
            this.uriFileName = uriFileName;
        }

        public String getEditPop() {
            return editPop;
        }

        public void setEditPop(String editPop) {
            this.editPop = editPop;
        }

        public String getResStr() {
            return resStr;
        }

        public void setResStr(String resStr) {
            this.resStr = resStr;
        }
    }
}
