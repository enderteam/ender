package com.pion.pro.eNDer.service;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.pion.pro.eNDer.ui.SignPadActivity;
import com.pion.pro.eNDer.util.Global;

/**
 * Created by joy on 2016. 9. 26..
 */

public class OnScreenshotEventReg implements Runnable, OnScreenshotTakenListener {

    private static final String TAG = "OnScreenshotEventReg";
    private UMLoggerService context;
    ScreenshotObserver observer;

    // 캡쳐 리스너로 전달되는 함수
    public void onScreenshotTaken(Uri uri){

        Log.e(TAG, "======================= onScreenshotTaken " + Global.IsSingPadActivity );
        if ( ! Global.IsSingPadActivity ) {
            Intent intent = new Intent(context, SignPadActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("URI", uri.toString());
//		startActivityForResult(intent,1);
            this.context.startActivity(intent);
        }
    }

    public OnScreenshotEventReg(UMLoggerService context) {
        this.context = context;
        observer = new ScreenshotObserver(this);
    }

    @Override
    public void run() {
        observer.start();
    }
}
