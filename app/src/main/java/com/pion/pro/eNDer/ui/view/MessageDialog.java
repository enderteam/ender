package com.pion.pro.eNDer.ui.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pion.pro.eNDer.R;

/**
 * Created by atam on 2016. 10. 19..
 */

public class MessageDialog extends Dialog implements View.OnClickListener
{
    private TextView titleTextView;
    private TextView messageTextView;

    private Button button1;

    private OnClickListener positiveButtonListener;

    private View buttonLayout;


    private MessageDialog(Context context) {
        this(context, 0);

    }

    private MessageDialog(Context context, int theme) {
        super(context, theme);
        //executeDelayed();
        initDialog();
    }

    @SuppressLint("InflateParams")
    private void initDialog() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);


        setContentView(getLayoutInflater().inflate(R.layout.view_message_dialog, null));
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        titleTextView = (TextView) findViewById(R.id.title);
        messageTextView = (TextView) findViewById(R.id.message);

        buttonLayout = findViewById(R.id.layout_button);

        button1 = (Button) findViewById(R.id.button1);

    }

    @Override
    public void setTitle(CharSequence title) {
        titleTextView.setText(title);
        titleTextView.setVisibility(View.VISIBLE);
    }

    public void setMessage(CharSequence message) {
        messageTextView.setText(message);
        messageTextView.setVisibility(View.VISIBLE);
    }

    public void setButton(int whichButton, CharSequence buttonText, OnClickListener buttonListener) {
        buttonLayout.setVisibility(View.VISIBLE);

        switch (whichButton) {
            case BUTTON_POSITIVE:
                button1.setText(buttonText);
                button1.setVisibility(View.VISIBLE);
                button1.setOnClickListener(this);
                positiveButtonListener = buttonListener;
                break;


            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                if (positiveButtonListener != null)
                    positiveButtonListener.onClick(this, BUTTON_POSITIVE);
                //executeDelayed();
                break;


            default:
                break;
        }
    }

    public static class Builder {
        private int theme;

        private Context context;

        private CharSequence title;
        private CharSequence message;

        private CharSequence positiveButtonText;

        private OnClickListener positiveButtonListener;

        private OnCancelListener cancelListener;
        private OnKeyListener keyListener;

        private boolean cancelable = true;
        private boolean adminLogin = false;

        public Builder(Context context) {
            this(context, false);
        }

        public Builder(Context context, boolean adminLogin) {
            this(context, adminLogin, 0);
        }

        public Builder(Context context, boolean adminLogin, int theme) {
            this.theme = theme;
            this.context = context;
            this.adminLogin = adminLogin;
        }

        public Builder setTitle(int titleId) {
            this.title = context.getString(titleId);
            return this;
        }

        public Builder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(int messageId) {
            this.message = context.getString(messageId);
            return this;
        }

        public Builder setMessage(CharSequence message) {
            this.message = message;
            return this;
        }

        public Builder setPositiveButton(int textId, final OnClickListener listener) {
            this.positiveButtonText = context.getString(textId);
            this.positiveButtonListener = listener;
            return this;
        }

        public Builder setPositiveButton(CharSequence text, final OnClickListener listener) {
            this.positiveButtonText = text;
            this.positiveButtonListener = listener;
            return this;
        }

        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder setOnCancelListener(OnCancelListener onCancelListener) {
            this.cancelListener = onCancelListener;
            return this;
        }

        public Builder setOnKeyListener(OnKeyListener onKeyListener) {
            this.keyListener = onKeyListener;
            return this;
        }

        public MessageDialog create() {
            final MessageDialog dialog = new MessageDialog(this.context, this.theme);
            dialog.getWindow().setGravity(Gravity.TOP);
            WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
            params.x = 0;
            params.y = 295;
            dialog.getWindow().setAttributes(params);

            if (TextUtils.isEmpty(this.title) == false)
                dialog.setTitle(this.title);
            if (TextUtils.isEmpty(this.message) == false)
                dialog.setMessage(this.message);

            if (positiveButtonText != null)
                dialog.setButton(BUTTON_POSITIVE, positiveButtonText, positiveButtonListener);

            dialog.setOnCancelListener(cancelListener);
            dialog.setOnKeyListener(keyListener);

            dialog.setCancelable(cancelable);

            return dialog;
        }
    }
}
