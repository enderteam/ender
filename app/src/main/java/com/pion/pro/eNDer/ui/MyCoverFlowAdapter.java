package com.pion.pro.eNDer.ui;

/**
 * Created by joy on 2016. 9. 26..
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pion.pro.eNDer.R;
import com.pion.pro.eNDer.util.EnderImgsData;
import com.pion.pro.eNDer.util.Global;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;


public class MyCoverFlowAdapter extends CoverFlowAdapter {

    private String TAG = "MyCoverFlowAdapter";
    private int imgCounter;
    //private String[] imgsData = null;
    private Context mContext;
    HashMap<Integer,Bitmap> imgsHash = new HashMap<Integer, Bitmap>();
    HashMap<Integer,String> appNameHash = new HashMap<Integer, String>();
    HashMap<Integer,String> appDescHash = new HashMap<Integer, String>();
    HashMap<Integer,String> imgsStr = new HashMap<Integer, String>();

    public MyCoverFlowAdapter(Context context) {


        mContext = context;
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String enderImgsData = mPreferences.getString("enderImgsData", "");
        if ( enderImgsData.equals("") ) {
            ((Activity)context).finish();  //메세지 처리.

        }
        Log.i(TAG,"======== enderImgsData : " + enderImgsData );
        Type collectionType = new TypeToken<List<EnderImgsData.ImageData>>(){}.getType();
        List<EnderImgsData.ImageData> lcs = (List<EnderImgsData.ImageData>) new Gson().fromJson( enderImgsData , collectionType);

//        imgsData = enderImgsData.split("\\|");
//        imgCounter = imgsData.length - 1;
        imgCounter = lcs.size();
        Global.logOut(TAG,"======== imgCounter : " + imgCounter );
        com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setCaptureCnt(String.valueOf(imgCounter));

        setBmp(lcs);

    }

    private void setBmp(List<EnderImgsData.ImageData> lcs) {

        Bitmap bm = null;
        String imgPath = "";
        int i=0;
        for (EnderImgsData.ImageData imgs : lcs) {
            imgPath = imgs.getUriFileName();
            imgsStr.put(i,imgPath);
            appNameHash.put(i,imgs.getBrand_nm());
            String descStr = "(CPU:MEM:Battery)\n" + imgs.getResStr() +"\n"+ imgs.getEditPop();
            appDescHash.put(i,descStr);

            try {
                if ( imgPathExist(imgPath) ) {
                    bm = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), getUriFromPath(imgPath));
                    imgsHash.put(i,bm);
                } else {
                    // 이미지가 삭제되었다면 대체 이미지로 바꿈
                    bm = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.intro_1);
                    imgsHash.put(i,bm);
                }


            } catch (IOException e) {
                e.printStackTrace();
            }

            i++;
        }

        if ( imgCounter == 1 ) {
            imgsHash.put(1,BitmapFactory.decodeResource(mContext.getResources(), R.drawable.intro_1));
            imgsStr.put(1,"");
            imgsHash.put(2,BitmapFactory.decodeResource(mContext.getResources(), R.drawable.intro_1));
            imgsStr.put(2,"");

        } else if ( imgCounter == 2 ) {
            imgsHash.put(2,BitmapFactory.decodeResource(mContext.getResources(), R.drawable.intro_1));
            imgsStr.put(2,"");
        }
    }

    private boolean imgPathExist(String imgPath) {

        Boolean rtn = false;
        // Uri : file:///storage/emulated/0/Pictures/Screenshots/Screenshot_2016-09-26-21-07-34.png
        String path = Global.Wonder_IMG_PATH + "/" + imgPath;

        File files = new File(path);
        if ( files.exists() == true ) {
            rtn = true;
        }

        Log.i(TAG,"======== file path =========================" + path + " / exist = " + rtn );
        return rtn;
    }

    public void changeBitmap() {
        //dataChanged = true;

        notifyDataSetChanged();
    }

//    @Override
//    public int getCount() {
//        return dataChanged ? 3 : 8;
//    }

    //String captureDate = uriFileName.split("_")[1].replace(".png","");
    //String preferStr = getPreStr + "|" + appName + "/" + uriFileName;

    @Override
    public int getCount() {
        int rtn = imgCounter;
        if ( rtn < 3 ) rtn = 3;
        return rtn;
    }

    @Override
    public Bitmap getImage(final int position) {

        //if ( bmpArray.length < position ) position = bmpArray.length;

        int pos = position;
        int hashSize = imgsHash.size();
        if ( pos < 0 || pos >= hashSize ) {
            pos = Math.abs(pos)%hashSize;
        }

        Global.logOut(TAG,"imgsHash.get(position) = " + pos );

        return imgsHash.get(pos);

    }

    public Uri getUriFromPath(String fileName) {

        // Uri : file:///storage/emulated/0/Pictures/Screenshots/Screenshot_2016-09-26-21-07-34.png
        //String path = Global.Wonder_IMG_PATH + "/" + fileName;
        String path = "file:///sdcard/Wonder/" + fileName;

        Log.i(TAG,"============================" + path );
        Uri fileUri = Uri.parse( path );

        return  fileUri;

//        String filePath = fileUri.getPath();
//        Cursor cursor = mContext.getContentResolver().query( MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                null, "_data = '" + filePath + "'", null, null );
//        cursor.moveToNext();
//        int id = cursor.getInt( cursor.getColumnIndex( "_id" ) );
//        Uri uri = ContentUris.withAppendedId( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id );
//        return uri;
    }
}
