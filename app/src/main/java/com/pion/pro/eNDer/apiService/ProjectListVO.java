package com.pion.pro.eNDer.apiService;

/**
 * DBtable : ATAM_USER_APP , ATAM_PROJECT
 * Create by Jungjoong.Kim
 * create date. 2016.09.26 
 * update date. 2016.09.26
 * */
public class ProjectListVO {

	private String project_nm;
	private String app_ver;
	private String user_app_seq;
	private String pkg_nm;
	private String activity_nm;
	private String crawl_status_div;
	private String crawl_status;
	private String user_seq;
	private String apk_nm;
	
	
	public String getApk_nm() {
		return apk_nm;
	}
	public void setApk_nm(String apk_nm) {
		this.apk_nm = apk_nm;
	}
	public String getProject_nm() {
		return project_nm;
	}
	public void setProject_nm(String project_nm) {
		this.project_nm = project_nm;
	}
	public String getApp_ver() {
		return app_ver;
	}
	public void setApp_ver(String app_ver) {
		this.app_ver = app_ver;
	}
	public String getUser_app_seq() {
		return user_app_seq;
	}
	public void setUser_app_seq(String user_app_seq) {
		this.user_app_seq = user_app_seq;
	}
	public String getPkg_nm() {
		return pkg_nm;
	}
	public void setPkg_nm(String pkg_nm) {
		this.pkg_nm = pkg_nm;
	}
	public String getActivity_nm() {
		return activity_nm;
	}
	public void setActivity_nm(String activity_nm) {
		this.activity_nm = activity_nm;
	}
	public String getCrawl_status_div() {
		return crawl_status_div;
	}
	public void setCrawl_status_div(String crawl_status_div) {
		this.crawl_status_div = crawl_status_div;
	}
	public String getCrawl_status() {
		return crawl_status;
	}
	public void setCrawl_status(String crawl_status) {
		this.crawl_status = crawl_status;
	}
	public String getUser_seq() {
		return user_seq;
	}
	public void setUser_seq(String user_seq) {
		this.user_seq = user_seq;
	}
	
	
	
}
