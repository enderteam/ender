package com.pion.pro.eNDer;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Process;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.pion.pro.eNDer.apiService.ServiceApiHttpClient;
import com.pion.pro.eNDer.ui.IntroActivity;
import com.pion.pro.eNDer.ui.SelectApp;
import com.pion.pro.eNDer.ui.view.MessageDialog;
import com.pion.pro.eNDer.util.ApiUrl;
import com.pion.pro.eNDer.util.Global;
import com.pion.pro.eNDer.util.PreferenceManager;
//import com.squareup.leakcanary.LeakCanary;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by atam on 2016. 10. 21..
 */

public class Application extends android.app.Application {

    private Thread.UncaughtExceptionHandler mUncaughtExceptionHandler;		//고려하지 않던 오류를 잡아주는 핸들러

    private static Application singleton;
    Context mContext;
    MessageDialog messageDialog;
    public static Application getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        mContext = this.getApplicationContext();
        mUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();

        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandlerApplication());

        disableKeyguard();	//잠금화면 해제


        super.onCreate();
        Log.d(Global.LOG_TAG, "Application onCreate()");
//        LeakCanary.install(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(Global.LOG_TAG, "Application onTerminate()");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.d(Global.LOG_TAG, "Application onLowMemory()");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(Global.LOG_TAG, "Application onConfigurationChanged()");
    }


    private void disableKeyguard() {
        KeyguardManager manager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = manager.newKeyguardLock(KEYGUARD_SERVICE);
        lock.disableKeyguard();	// 잠금화면 해제의 역할
        //lock.reenableKeyguard();	//해제된 잠금화면을 다시 원상태로 돌리는 기능
    }


    class UncaughtExceptionHandlerApplication implements Thread.UncaughtExceptionHandler { // application에 대해 정정되지 않은 오류 발생시 예외 처리
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            Log.d(Global.LOG_TAG, "Wonder Application Error : \n" + ex);

//            MessageDialog.Builder alert = new MessageDialog.Builder(mContext)
//                    .setTitle("WONDER Exception")
//                    .setMessage("Exception : \n" + ex.toString())
//                    .setCancelable(false)
//                    .setPositiveButton("ReStart", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
                            // Crash 발생 시 어플리케이션을 재 시작
            restartApplication(ex);
//          // Crash 발생 시 앱 죽임
//            appTaskKill();
//                            dialog.dismiss();
//                        }
//                    });
//            messageDialog = alert.create();
//            messageDialog.show();


            mUncaughtExceptionHandler.uncaughtException(thread, ex);
        }
    }

    private void appTaskKill(){
        ((Activity)mContext).finishAndRemoveTask();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
//				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        ActivityCompat.finishAffinity(Application.this);
        System.runFinalizersOnExit(true);
//        moveTaskToBack(true);
//        finish();
    }

    /**
     * 어플리케이션 재 시작
     */
    private void restartApplication(final Throwable ex) {

        PendingIntent myActivity = PendingIntent.getActivity(getBaseContext(), 0, new Intent(getBaseContext(),
                IntroActivity.class), PendingIntent.FLAG_ONE_SHOT);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                // Error Toast show !!
                Looper.prepare();
                Toast.makeText(mContext, "WONDER Service Stop ..!!", Toast.LENGTH_LONG).show();
                Looper.loop();
            }
        });
        thread.setDaemon(true);
        thread.start();

        /*new Thread() {
            @Override
            public void run() {
                // Error Toast show !!
                Looper.prepare();
                Toast.makeText(mContext, "WONDER Service Stop .. Restart Application !!", Toast.LENGTH_LONG).show();
                Looper.loop();
            }
        }.start();*/

        try {
            if (PreferenceManager.getInstance(mContext) != null){ UpdateAtamEnderIsNull(ex);}

            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

//        AlarmManager alarmManager;
//        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, myActivity);

        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            ((Activity)mContext).finishAndRemoveTask();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
//				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//        ActivityCompat.finishAffinity(Application.this);
            System.runFinalizersOnExit(true);

            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(10);
        }
    }

    private void UpdateAtamEnderIsNull(final Throwable ex) {

        final Map maps = new HashMap();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String result = "";
                try {
                    // HTTP 요청 준비 작업
                    ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
                            "http://" + Global.getBaseUrl() + ApiUrl.API_NUM_10);

                    maps.put("user_seq", PreferenceManager.getInstance(mContext).getUserSeq());
                    maps.put("have_device_seq", PreferenceManager.getInstance(mContext).getHaveDeviceSeq());
                    maps.put("end_date", PreferenceManager.getInstance(mContext).getEndDate());
                    // 파라미터를 전송한다.
                    http.addAllParameters(maps);
                    //http.addOrReplace("DATA","{"+maps[0]+"}");

                    // HTTP 요청 전송
                    ServiceApiHttpClient post = http.create();
                    post.request();

                    // 응답 상태코드 가져오기
                    int statusCode = post.getHttpStatusCode();

                    // 응답 본문 가져오기
                    String body = post.getBody();
                    Global.logOut("ApiUrl.API_NUM_10 : " + body);
                    // 실패 -> {"state":"004"}
                    //여기서 부터 Json으로 파싱해서 사용해야함

                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    Log.w(Global.LOG_TAG, "Serice All Stop!!!!!" + " ex : " + ex.toString());
                    SelectApp.counterService = null;
                    if(SelectApp.serviceIntent != null){stopService(SelectApp.serviceIntent);}
                    if(SelectApp.conn != null) getApplicationContext().unbindService(SelectApp.conn);
                    if(SelectApp.serviceIntent != null){
                        getApplicationContext().bindService(SelectApp.serviceIntent, SelectApp.conn, 0);
                    }
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

        /*new Thread() {
            public void run() {
                String result = "";
                try {
                    // HTTP 요청 준비 작업
                    ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
                            "http://" + Global.getBaseUrl() + ApiUrl.API_NUM_10);

                    maps.put("user_seq", PreferenceManager.getInstance(mContext).getUserSeq());
                    maps.put("have_device_seq", PreferenceManager.getInstance(mContext).getHaveDeviceSeq());
                    maps.put("end_date", PreferenceManager.getInstance(mContext).getEndDate());
                    // 파라미터를 전송한다.
                    http.addAllParameters(maps);
                    //http.addOrReplace("DATA","{"+maps[0]+"}");

                    // HTTP 요청 전송
                    ServiceApiHttpClient post = http.create();
                    post.request();

                    // 응답 상태코드 가져오기
                    int statusCode = post.getHttpStatusCode();

                    // 응답 본문 가져오기
                    String body = post.getBody();
                    Log.e(Global.LOG_TAG, "ApiUrl.API_NUM_10 : " + body);
                    // 실패 -> {"state":"004"}
                    //여기서 부터 Json으로 파싱해서 사용해야함

                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    Log.w(Global.LOG_TAG, "Serice All Stop!!!!!" + " ex : " + ex.toString());
                    SelectApp.counterService = null;
                    if(SelectApp.serviceIntent != null){stopService(SelectApp.serviceIntent);}
                    if(SelectApp.conn != null) getApplicationContext().unbindService(SelectApp.conn);
                    if(SelectApp.serviceIntent != null){
                        getApplicationContext().bindService(SelectApp.serviceIntent, SelectApp.conn, 0);
                    }
                }
            }
        }.start();*/

    }
}
