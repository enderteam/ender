package com.pion.pro.eNDer.ui.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pion.pro.eNDer.R;

/**
 * Created by atam on 2016. 10. 19..
 */

public class InputDialog extends Dialog implements View.OnClickListener
{
    private TextView titleTextView;
    private TextView messageTextViewCPU;
    private TextView messageTextViewMEM;
    private TextView messageTextViewBAT;
    private EditText checkInput;
//	private EditText pwdEditText;

    private Button button1;//, button2


    private OnClickListener positiveButtonListener;
    private OnClickListener negativeButtonListener;
    private View buttonLayout;

    private InputDialog(Context context) {
        this(context, 0);

    }
    private InputDialog(Context context, int theme) {
        super(context, theme);
        initDialog();
    }


    @SuppressLint("InflateParams")
    private void initDialog() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(getLayoutInflater().inflate(R.layout.view_input_dialog, null));
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        executeDelayed();
        titleTextView = (TextView) findViewById(R.id.title_template);
        checkInput = (EditText) findViewById(R.id.checkInput);
        messageTextViewCPU = (TextView) findViewById(R.id.message_cpu);
        messageTextViewMEM = (TextView) findViewById(R.id.message_memory);
        messageTextViewBAT = (TextView) findViewById(R.id.message_battery);
        buttonLayout = findViewById(R.id.layout_button);

        button1 = (Button) findViewById(R.id.button1);
//        button2 = (Button) findViewById(R.id.btn_exit);

    }



    @Override
    public void setTitle(CharSequence title) {
        titleTextView.setText(title);
        titleTextView.setVisibility(View.VISIBLE);
    }

    public void setMessageCPU(CharSequence message) {
        messageTextViewCPU.setText(message);
        messageTextViewCPU.setVisibility(View.VISIBLE);
    }

    public void setMessageMEM(CharSequence message) {
        messageTextViewMEM.setText(message);
        messageTextViewMEM.setVisibility(View.VISIBLE);
    }

    public void setMessageBAT(CharSequence message) {
        messageTextViewBAT.setText(message);
        messageTextViewBAT.setVisibility(View.VISIBLE);
    }

    public String getCheckInput() {
        return checkInput.getText().toString();
    }

    public void setButton(int whichButton, CharSequence buttonText, OnClickListener buttonListener) {
        buttonLayout.setVisibility(View.VISIBLE);

        switch (whichButton) {
            case BUTTON_POSITIVE:
                button1.setText(buttonText);
                button1.setVisibility(View.VISIBLE);
                button1.setOnClickListener(this);
                positiveButtonListener = buttonListener;
                break;

//            case BUTTON_NEGATIVE:
//                button2.setText(buttonText);
//                button2.setVisibility(View.VISIBLE);
//                button2.setOnClickListener(this);
//                negativeButtonListener = buttonListener;
//                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                if (positiveButtonListener != null)
                    positiveButtonListener.onClick(this, BUTTON_POSITIVE);
                break;
//            case R.id.btn_exit:
//                if (negativeButtonListener != null)
//                    negativeButtonListener.onClick(this, BUTTON_NEGATIVE);
//                break;
            default:
                break;
        }
    }

    public static class Builder {
        private int theme;

        private Context context;

        private CharSequence title;
        private CharSequence messageCPU;
        private CharSequence messageMEM;
        private CharSequence messageBAT;

        private CharSequence positiveButtonText;
//        private CharSequence negativeButtonText;

        private OnClickListener positiveButtonListener;
//        private OnClickListener negativeButtonListener;

        private OnCancelListener cancelListener;
        private OnKeyListener keyListener;

        private boolean cancelable = true;

        public Builder(Context context) {
            this(context, 0);
        }

        public Builder(Context context, int theme) {
            this.theme = theme;
            this.context = context;
        }

        public Builder setTitle(int titleId) {
            this.title = context.getString(titleId);
            return this;
        }

        public Builder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public Builder setMessageCPU(int messageId) {
            this.messageCPU = context.getString(messageId);
            return this;
        }

        public Builder setMessageCPU(CharSequence message) {
            this.messageCPU = message;
            return this;
        }

        public Builder setMessageMEM(int messageId) {
            this.messageMEM = context.getString(messageId);
            return this;
        }

        public Builder setMessageMEM(CharSequence message) {
            this.messageMEM = message;
            return this;
        }

        public Builder setMessageBAT(int messageId) {
            this.messageBAT = context.getString(messageId);
            return this;
        }

        public Builder setMessageBAT(CharSequence message) {
            this.messageBAT = message;
            return this;
        }

        public Builder setPositiveButton(int textId, final OnClickListener listener) {
            this.positiveButtonText = context.getString(textId);
            this.positiveButtonListener = listener;
            return this;
        }

        public Builder setPositiveButton(CharSequence text, final OnClickListener listener) {
            this.positiveButtonText = text;
            this.positiveButtonListener = listener;
            return this;
        }

//        public Builder setNegativeButton(int textId, final OnClickListener listener) {
//            this.negativeButtonText = context.getString(textId);
//            this.negativeButtonListener = listener;
//            return this;
//        }
//
//        public Builder setNegativeButton(CharSequence text, final OnClickListener listener) {
//            this.negativeButtonText = text;
//            this.negativeButtonListener = listener;
//            return this;
//        }


        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder setOnCancelListener(OnCancelListener onCancelListener) {
            this.cancelListener = onCancelListener;
            return this;
        }

        public Builder setOnKeyListener(OnKeyListener onKeyListener) {
            this.keyListener = onKeyListener;
            return this;
        }

        public InputDialog create() {
            final InputDialog dialog = new InputDialog(this.context, this.theme);
            WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
            params.x = 0;
            params.y = 279; //279
            dialog.getWindow().setAttributes(params);
            dialog.getWindow().setGravity(Gravity.TOP);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

            if (TextUtils.isEmpty(this.title) == false)
                dialog.setTitle(this.title);
            if (TextUtils.isEmpty(this.messageCPU) == false)
                dialog.setMessageCPU(this.messageCPU);
            if (TextUtils.isEmpty(this.messageMEM) == false)
                dialog.setMessageMEM(this.messageMEM);
            if (TextUtils.isEmpty(this.messageBAT) == false)
                dialog.setMessageBAT(this.messageBAT);

            if (positiveButtonText != null)
                dialog.setButton(BUTTON_POSITIVE, positiveButtonText, positiveButtonListener);
//            if (negativeButtonText != null)
//                dialog.setButton(BUTTON_NEGATIVE, negativeButtonText, negativeButtonListener);

            dialog.setOnCancelListener(cancelListener);
            dialog.setOnKeyListener(keyListener);

            dialog.setCancelable(cancelable);

            return dialog;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        executeDelayed();
    }

    //hide Navigation Bar
    public void executeDelayed() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideNavBar();
            }
        }, 0);
    }

    public void hideNavBar() {
        View v = getWindow().getDecorView();
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
