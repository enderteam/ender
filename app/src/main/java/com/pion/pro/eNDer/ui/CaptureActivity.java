package com.pion.pro.eNDer.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.OrientationEventListener;
import android.widget.Toast;

import com.pion.pro.eNDer.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class CaptureActivity extends Activity {
    // 종료와 시작을 정의하기 위해서 전역으로 정의해주었습니다.
    /**
     * MediaProjection을 사용하기 위한 객체입니다.
     */
    private MediaProjection mediaProjection;
    private MediaProjectionManager mMediaProjectionManager;
    /**
     * 실제 화면의 크기를 정하고, Surface를 통해 화면을 그리게 됩니다
     */
    private VirtualDisplay virtualDisplay;

    //
    private static final String TAG = "ScreenTestCapture";
    private static final int REQUEST_CODE = 100;
    private static String STORE_DIRECTORY;
    private static int IMAGES_PRODUCED;
    private static final String SCREENCAP_NAME = "screencap";
    private static final int VIRTUAL_DISPLAY_FLAGS = DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY | DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC;
    private static MediaProjection sMediaProjection;


    private MediaProjectionManager mProjectionManager;
    private ImageReader mImageReader;
    private Handler mHandler;
    private Display mDisplay;
    private VirtualDisplay mVirtualDisplay;
    private int mDensity;
    private int mWidth;
    private int mHeight;
    private int mRotation;
    private OrientationChangeCallback mOrientationChangeCallback;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);

        /*
        서비스를 먼저 받아옵니다. 요게 이제 멤버변수 MediaProjectionManager로 들어갑니다.
         */
        mProjectionManager = (MediaProjectionManager)getSystemService(Context.MEDIA_PROJECTION_SERVICE);


        /*
        여기서 mHandler가 만들어지게 됩니다.
        새로운 스레드 하나 만들고, 핸들러 만들어서 prepare()로 메세지큐가 준비되면, 핸들러 만들고
        loop로 이제 무한정 기다리게 된다네요. 해당 스레드를 사용할때는 성능상 문제가 없도록
        구현해야 된다고 합니다. 강제종료를 시키지 않으면, 메모리를 계속 차지하고 있으니까요.
        그런데 아직도 핸들러 개념이 어렵습니다. 위에서 말했던 것과 다르게 엄...
        스레드가 충돌 나는 부분이라기도 애매한게 여기서 새로운 스레드를 만든건데...아...
        모르겠습니다 그냥 패스. 책 보면서 추가 공부 해야되겠습니다.
         */
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                mHandler = new Handler();
                Looper.loop();
            }
        });
        thread.setDaemon(true);
        thread.start();
//        new Thread() {
//            @Override
//            public void run() {
//                Looper.prepare();
//                mHandler = new Handler();
//                Looper.loop();
//            }
//        }.start();



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
        권한 요청하고 REQUEST_CODE는 임의 코드 입니다.
         */
        if (requestCode == REQUEST_CODE) {
            //resultCode와Intent를getMediaProjection에 넘겨주고 sMediaProjection에 들어갑니다.
            //권한 부여 받고나면, 끝이니 static object에 넣어놓은 것 같기도 하고요.
            //여러개 만들 필요가 없어서??... 새로운게 만들어지면 전에 있던걸 사용 못하는건가? 아니
            //그것도 불필요한 행위인지...잘 모르겠습니다. 쨋든 이런 구조네요.
            sMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);

            if (sMediaProjection != null) {
                /*
                getMediaProjection으로 object가 잘 만들어졌으면 여기서 이제 저장할 driectory 생성
                합니다.
                 */
                STORE_DIRECTORY = Environment.getExternalStorageDirectory() + "/data/capturetest/";
                File storeDirectory = new File(STORE_DIRECTORY);
                if (!storeDirectory.exists()) {
                    boolean success = storeDirectory.mkdirs();
                    if (!success) {
                        return;
                    }
                }
            }

            /*
            현재 디스플레이의 density dpi 가져 옵니다.
             */
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            mDensity = metrics.densityDpi;
            mDisplay = getWindowManager().getDefaultDisplay();

            /*
            그리고 나서 createVirtualDisplay() 호출해서 virtualdisplay를 만듭니다.
             */
            createVirtualDisplay();

            /*
            여건  orientation callback 등록 부분.
            감지 할 수 있ㅇ면, enable()로 이제 감지하게끔 해주는가 봅니다.
             */
            mOrientationChangeCallback = new OrientationChangeCallback(this);
            if (mOrientationChangeCallback.canDetectOrientation()) {
                mOrientationChangeCallback.enable();
            }

            //getMdiaProjection으로 가져온 object에 이제register 등록을 해줍니다. 핸들러와 함께
            //흠... mHandler를 여기서등록 시켜주면, 흠..null값을 줘도 된다고 developer에 나와있습니다.
            //looper를 호출 할 필요가 있다면 핸들러를 넣으려는데 아직도 handler를 쓴 이유를 잘 모르겠네요.
            sMediaProjection.registerCallback(new MediaProjectionStopCallback(), mHandler);
        }

    }

    /*
       ImageReader에서 Image를 처리할 class 입니다.
       OnImageAvailableListener를 상속받아 구현 되었으며, onImageAvailable(ImageReader) 메소드가
       오버라이딩 되었고, 해당 메소드 내에서 이미지를 꺼내 처리하믄 되나 봅니다.
    */
    private class ImageAvailableListener implements ImageReader.OnImageAvailableListener {
        @Override
        public void onImageAvailable(ImageReader imageReader) {
            Image image = null;
            FileOutputStream fos = null;
            Bitmap bitmap = null;

            try {
                //가장 최신 이미지를 가져 옵니다. image 객체로
                image = mImageReader.acquireLatestImage();
                if (image != null) {
                    /*
                    여기서 getPlanes()로 가져와서 0배열만 쓰는 이유를 모르겠습니다.
                    혹시 다차원 이미지도 지원되기때문에 그런건 아닐까 하는 생각을 조심스럽게 해봅니다.
                    그래서 2차원 평면만 가져온건 아닌지...는 모르겠고 그냥 모르겠습니다.
                    쨋든 이미지 버퍼정보와.... 픽셀하고 행??...보폭이라 표현한건 단위인건가??
                    rowPadding 패딩값을 알기위해 행단위 - 픽셀*너비?? 이게 밑에서
                    너비부분만 사용할 때 사용되는데 이유를 모르겠습니다. 왜 이런 수식을 쓰는지
                    (mWidth + (rowStride - pixelStride * mWidth) / pixelStride)
                    인건데... 아 모르겠다. 안드로이드 공부가 너무 부족한 듯 합니다.
                    이 api 공부 이후로는 안드로이드 기본이나 공부를 더 해야겠어요.
                    */
                    Image.Plane[] planes = image.getPlanes();
                    ByteBuffer buffer = planes[0].getBuffer();
                    int pixelStride = planes[0].getPixelStride();
                    int rowStride = planes[0].getRowStride();
                    int rowPadding = rowStride - pixelStride * mWidth;

                    //쨋든 createBitmap으로 bitmap파일 만들고 위의 이미지 buffer로 이미지를 가져옵니다.
                    bitmap = Bitmap.createBitmap(mWidth + rowPadding / pixelStride, mHeight, Bitmap.Config.ARGB_8888);
                    bitmap.copyPixelsFromBuffer(buffer);

                    //그 다음 저장하는 부분
                    fos = new FileOutputStream(STORE_DIRECTORY + "/myscreen_" + IMAGES_PRODUCED + ".jpg");
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

                    IMAGES_PRODUCED++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }

                if (bitmap != null) {
                    bitmap.recycle();
                }

                if (image != null) {
                    image.close();
                }
            }
        }
    }


    /*
    제가보기엔 rotation시 사용할 클래스 인것 같습니다.말고도 추가적으로 change 되는 부분이 있나
    정위로..?
     */
    private class OrientationChangeCallback extends OrientationEventListener {
        //생성자가 필히 요구 됩니다.
        public OrientationChangeCallback(Context context) {
            super(context);
        }

        @Override
        public void onOrientationChanged(int orientation) {
            synchronized (this) {
                //화면 전환으로 인해서 virtualdisplay를 새로만드는 과정이니 동기화 시켜주고
                final int rotation = mDisplay.getRotation();

                //rotation값이다르다면
                if (rotation != mRotation) {
                    mRotation = rotation;
                    try {
                        /*
                        virtualdisplay를 release 해주고 imagereader의 이벤트도 빼버리고 그런데
                        imagereader 이벤트 빼고나서 null로 주어야되지않나? 알아서 가비지컬렉터가
                        메모리 해제해주는지 모르겠네요. newInstance로 새롭게 객체 만들텐데
                        쨋든 그러고나서 createVirtualDisplay로 virtualdisplay 새로 생성
                         */
                        if (mVirtualDisplay != null) mVirtualDisplay.release();
                        if (mImageReader != null) mImageReader.setOnImageAvailableListener(null, null);

                        createVirtualDisplay();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    /*
       MediaProjection callback class 입니다.
       MediaProjection.Callback class를 extends 했고요.
       onStop() 메소드만 오버라이딩 되 있습니다.
       이제 여기서 또한 mHandler가 사용되게됩니다.
       그런데 변수만 다루는 부분인데, 굳이 핸들러를 사용해야됬을지 모르겠습니다.
       혹시 stopProjection 이라든지 다른 메모리해제 하는 프로세싱과 충돌을 일으킬까봐
       한 핸들러에서 메세지큐에 넣고 처리한다든지?! 그런데 이부분은 동기화로 가능한 부분아닌가.
       저도 잘 모르겠습니다. handler로 구현한걸 봐서 handler로 구현하지 않으면 안되는 부분
       인거같기도하고...
     */
    private class MediaProjectionStopCallback extends MediaProjection.Callback {
        @Override
        public void onStop() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mVirtualDisplay != null) mVirtualDisplay.release();
                    if (mImageReader != null) mImageReader.setOnImageAvailableListener(null, null);
                    if (mOrientationChangeCallback != null) mOrientationChangeCallback.disable();
                    sMediaProjection.unregisterCallback(MediaProjectionStopCallback.this);
                }
            });
        }
    }

    public void startProjection() {
        //사용자 허가 요청!
        try{
            if (mProjectionManager == null){
                /*
                서비스를 먼저 받아옵니다. 요게 이제 멤버변수 MediaProjectionManager로 들어갑니다.
                 */
                mProjectionManager = (MediaProjectionManager)getSystemService(Context.MEDIA_PROJECTION_SERVICE);
                startActivityForResult(mProjectionManager.createScreenCaptureIntent(), REQUEST_CODE);

            }else{
                startActivityForResult(mProjectionManager.createScreenCaptureIntent(), REQUEST_CODE);
            }

        }catch (NoClassDefFoundError ex){
            ex.printStackTrace();
//            Toast.makeText(this, "호환되지 않는 디바이스 입니다.", Toast.LENGTH_LONG);
        }

    }

    public void stopProjection() {
        //projection을 종료 합니다. stop()! mediaprojection callback의
        //onstop method가 호출 되겠네요.
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (sMediaProjection != null) {
                    sMediaProjection.stop();
                }
            }
        });
    }


    //가상 디스플레이를 만듭니다.
    private void createVirtualDisplay() {
        //가로,세로 고려 사이즈는 다시 설정하고
        Point size = new Point();
        mDisplay.getSize(size);
        mWidth = size.x;
        mHeight = size.y;

        //ImageReader 새로운 사이즈의 인스턴스 만들고, createVirtualDisplay로 생ㅇ성 합니다.
        //하고 이미지 처리할 ImageAvailableListener를 등록 해줍니다.
        mImageReader = ImageReader.newInstance(mWidth, mHeight, PixelFormat.RGBA_8888, 2);
        mVirtualDisplay = sMediaProjection.createVirtualDisplay(SCREENCAP_NAME, mWidth, mHeight, mDensity, VIRTUAL_DISPLAY_FLAGS, mImageReader.getSurface(), null, mHandler);
        mImageReader.setOnImageAvailableListener(new ImageAvailableListener(), mHandler);
    }






//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        //ここも282でべた書き
//        if (282 == requestCode) {
//            if (resultCode != RESULT_OK) {
//                //パーミッションなし
//                Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
//                return;
//            }
//             // MediaProjectionの取得
//            mediaProjection =
//                    mMediaProjectionManager.getMediaProjection(resultCode, intent);
//
//            DisplayMetrics metrics = getResources().getDisplayMetrics();
//            int mWidth = metrics.widthPixels;
//            int mHeight = metrics.heightPixels;
//            int density = metrics.densityDpi;
//
//            Log.d("Log","setup VirtualDisplay");
//            mImageReader = ImageReader
//                    .newInstance(mWidth, mHeight, ImageFormat.RGB_565, 2);
//            virtualDisplay = mediaProjection
//                    .createVirtualDisplay("Capturing Display",
//                            mWidth, mHeight, density,
//                            DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
//                            mImageReader.getSurface(), null, null);
//        }
//
//    }


}
