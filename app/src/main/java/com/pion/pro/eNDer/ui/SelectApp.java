package com.pion.pro.eNDer.ui;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pion.pro.eNDer.R;
import com.pion.pro.eNDer.apiService.PackageInfo;
import com.pion.pro.eNDer.apiService.ProjectListVO;
import com.pion.pro.eNDer.apiService.ServiceApiHttpClient;
import com.pion.pro.eNDer.service.ICounterService;
import com.pion.pro.eNDer.service.UMLoggerService;
import com.pion.pro.eNDer.ui.view.AnimActivity;
import com.pion.pro.eNDer.ui.view.ListDialog;
import com.pion.pro.eNDer.util.ApiUrl;
import com.pion.pro.eNDer.util.Global;
import com.pion.pro.eNDer.util.SystemInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectApp extends AnimActivity {
	List<ApplicationInfo> appInfoList;
	private SharedPreferences mPreferences;
	ArrayList<String> localArrayList;
	List<Integer> targetAppUidList;
	List<Integer> uidList;
	List<Integer> uidListSeq = new ArrayList<Integer>();
	List<String> apkNmList;
	List<String> userAppSeqlist;
	List<String> proNmList;

	Context context;
	PackageManager pm;
	boolean isOneApp;

	MyTask myTask ;
	Intent intent ;
	public static Map<String, String> hashMap;
	LinearLayout container;

	public static Intent serviceIntent;
	public static ICounterService counterService;
	public static CounterServiceConnection conn;

	Handler overlayHandler;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.selectapp);
//		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);
		Global.IsSingPadActivity = true;
		UmLoggerOverlayViewVisivility(1);
		container = (LinearLayout)findViewById(R.id.container);
//		container.setBackgroundColor(0xAAFFFFFF);
		container.setBackgroundResource(R.drawable.bg_login);

		context = SelectApp.this;
		intent = getIntent();
		mPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		pm = getPackageManager();

		isOneApp = mPreferences.getBoolean("one_app", true);

		serviceIntent = new Intent(this, UMLoggerService.class);
		conn = new CounterServiceConnection();


		myTask = new MyTask(intent.getStringExtra("user_seq"));
		Map params = new HashMap();
		Global.logOut("SelectApp", "user_seq : " +  com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).getUserSeq());
		params.put("user_seq", intent.getStringExtra("user_seq") != null ? intent.getStringExtra("user_seq") : com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).getUserSeq());

		myTask.execute(params);

	}

	@Override
	protected void onResume() {
		super.onResume();
		Global.CaptureYn = false;
		UmLoggerOverlayViewVisivility(1);
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				getApplicationContext().bindService(serviceIntent, conn, 0);
			}
		}, 250);
	}

	@Override
	protected void onStop() {
		super.onStop();
		Global.IsSingPadActivity = false;
		UmLoggerOverlayViewVisivility(0);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (conn != null){
			getApplicationContext().unbindService(conn);
		}
		Global.IsSingPadActivity = false;
		UmLoggerOverlayViewVisivility(0);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Global.IsSingPadActivity = false;
		UmLoggerOverlayViewVisivility(0);
	}

	@Override
	protected void onUserLeaveHint() {
		super.onUserLeaveHint();
		Global.logOut("SelectApp", "Home Button Touch");
		UmLoggerOverlayViewVisivility(1);

//		Toast.makeText(context, "We are currently testing. It is not recommended to leave the screen.", Toast.LENGTH_LONG).show();
//		Intent intent = new Intent(SelectApp.this, SelectApp.class);
//		intent.addCategory(Intent.CATEGORY_HOME);
//		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//		startActivity(intent);
//		moveTaskToBack(true);
//		finish();
	}

	private void UmLoggerOverlayViewVisivility(final int visivility){
		overlayHandler = new Handler();
		overlayHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if(visivility == 0){
					if (UMLoggerService.oView != null){
						UMLoggerService.oView.setVisibility(View.VISIBLE);
						UMLoggerService.oView.setEnabled(true);
					}
				}else if (visivility == 1){
					if (UMLoggerService.oView != null){
						UMLoggerService.oView.setVisibility(View.INVISIBLE);
						UMLoggerService.oView.setEnabled(false);
					}
				}else if (visivility == 2){
					UMLoggerService.oView.setVisibility(View.INVISIBLE);
					UMLoggerService.oView.setEnabled(false);
				}
			}
		}, 2000);

	}

	private void getExeDeviceSeq(){


		final Map maps = new HashMap();

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				String result = "";
				try {
					// HTTP 요청 준비 작업
					ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
							"http://" + Global.getBaseUrl() + ApiUrl.API_NUM_06);

					maps.put("request", "eNDer");       //file error num

					// 파라미터를 전송한다.
					http.addAllParameters(maps);
					//http.addOrReplace("DATA","{"+maps[0]+"}");

					// HTTP 요청 전송
					ServiceApiHttpClient post = http.create();
					post.request();

					// 응답 상태코드 가져오기
					int statusCode = post.getHttpStatusCode();

					// 응답 본문 가져오기
					String body = post.getBody();
					//Global.logOut("ApiUrl.API_NUM_06 : " + body);       // 성공 -> {"exe_device_seq":"1","state":"001"}
					// 실패 -> {"state":"004"}
					//여기서 부터 Json으로 파싱해서 사용해야함

					JSONObject jsonObject = new JSONObject(body);
					result = jsonObject.getString("mirror_exe_seq");
					Global.logOut("ApiUrl.API_NUM_06 : " + result);
					com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).setExeDeviceSeq(result);



				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		thread.setDaemon(true);
		thread.start();

	}

	private void atamEnderInsert(){


		final Map maps = new HashMap();
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {

			}
		});
		thread.setDaemon(true);
		thread.start();
		new Thread() {
			public void run() {
				String result = "";
				try {
					// HTTP 요청 준비 작업
					ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
							"http://" + Global.getBaseUrl() + ApiUrl.API_NUM_06_1);

					maps.put("exe_device_seq", com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).getHaveDeviceSeq());
					maps.put("user_seq", com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).getUserSeq());
					maps.put("user_app_seq", com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).getUserAppSeq());

					// 파라미터를 전송한다.
					http.addAllParameters(maps);
					//http.addOrReplace("DATA","{"+maps[0]+"}");

					// HTTP 요청 전송
					ServiceApiHttpClient post = http.create();
					post.request();

					// 응답 상태코드 가져오기
					int statusCode = post.getHttpStatusCode();

					// 응답 본문 가져오기
					String body = post.getBody();
					Global.logOut("ApiUrl.API_NUM_06_1 : " + body);
					// 실패 -> {"state":"004"}
					//여기서 부터 Json으로 파싱해서 사용해야함


				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();


	}

	// Sort App List
	class NewComparator implements Comparator<ApplicationInfo> {
		public int compare(ApplicationInfo appInfo1, ApplicationInfo appInfo2) {
			SystemInfo sysInfo = SystemInfo.getInstance();
			PackageManager pm = getPackageManager();
			return sysInfo.getUidName(appInfo1.uid, pm).compareTo(
					sysInfo.getUidName(appInfo2.uid, pm));
//			return String.format("%d", appInfo1.uid).compareTo(String.format("%d", appInfo2.uid));
		}
	}

	class MyTask extends AsyncTask<Map, Void, String> {
		ProgressDialog progressDialog;
		private final String mUser_seq;
		private ListDialog dfDialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(SelectApp.this);
//			progressDialog.setTitle("잠시만 기다려주세요");
//			progressDialog.setMessage("설치된 어플리케이션 목록을 확인중..");
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.show();
			progressDialog.setContentView(R.layout.view_progressdialog_custom);
			progressDialog.setCancelable(true);
			progressDialog.setMax(500 - 1);
		}

		MyTask(String user_seq) {
			mUser_seq = user_seq;
		}

		@Override
		protected String doInBackground(Map... maps) {
			String selectedApps;
			// HTTP 요청 준비 작업
			ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
					"http://"+ Global.getBaseUrl()+ ApiUrl.API_NUM_04);

			// 파라미터를 전송한다.
			http.addAllParameters(maps[0]);
//            http.addOrReplace("DATA","{"+maps[0]+"}");

			// HTTP 요청 전송
			ServiceApiHttpClient post = http.create();
			post.request();

			// 응답 상태코드 가져오기
			int statusCode = post.getHttpStatusCode();

			// 응답 본문 가져오기
			String body = post.getBody();
			Global.logOut("ApiUrl.API_NUM_04 : " + body);
			List<PackageInfo> list = new ArrayList<PackageInfo>();
			apkNmList = new ArrayList<String>();
			userAppSeqlist = new ArrayList<String>();
			hashMap = new HashMap<>();

			try{
				JSONObject jsonObject = new JSONObject(body);
				JSONArray jsonArray = new JSONArray(jsonObject.getString("Data"));
				PackageInfo packageInfo = null;

				if(jsonObject != null && !jsonObject.equals("")) {

					String state = jsonObject.getString("state");
//						if(state == "002") {        //server에 앱이 등록 안되어져 있는 경우
//							Toast.makeText(context, "Available test application does not exist. Please ask your server administrator!!", Toast.LENGTH_LONG).show();
//							moveTaskToBack(true);
//							finish();
//							return body;
//						}else{
					packageInfo = new PackageInfo();
					JSONObject jsonObject1;
					ProjectListVO projectListVO = new ProjectListVO();

					for (int k = 0; k < jsonArray.length(); k++) {

						jsonObject1 = new JSONObject(jsonArray.getString(k));
						projectListVO.setActivity_nm(jsonObject1.getString("activity_nm"));
						projectListVO.setProject_nm(jsonObject1.getString("project_nm"));
						projectListVO.setApp_ver(jsonObject1.getString("app_ver"));
						projectListVO.setApk_nm(jsonObject1.getString("apk_nm"));
//						projectListVO.setCrawl_status_div(jsonObject1.getString("crawl_status_div"));
						projectListVO.setCrawl_status(jsonObject1.getString("crawl_status"));
						projectListVO.setUser_app_seq(jsonObject1.getString("user_app_seq"));
						projectListVO.setUser_seq(jsonObject1.getString("user_seq"));
						packageInfo.setData(projectListVO);
//
						apkNmList.add(jsonObject1.getString("apk_nm"));
						userAppSeqlist.add(jsonObject1.getString("user_app_seq"));
						list.add(packageInfo);
						hashMap.put(jsonObject1.getString("apk_nm"), jsonObject1.getString("user_app_seq"));


					}
//						}
				}

				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				SharedPreferences.Editor edit = prefs.edit();

				JSONArray array = new JSONArray();
				for (int i = 0; i < apkNmList.size(); i++)
				{
					array.put(apkNmList.get(i));
//					Log.d("SelectApp", "Server packageNM List : " + apkNm_List.get(i));
				}
				edit.putString("pkg_nm", array.toString());
				edit.commit();


				localArrayList = new ArrayList<String>();
				uidList = new ArrayList<Integer>();


//			appInfoList = pm.getInstalledApplications(PackageManager.GET_META_DATA);
				appInfoList = pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);


				selectedApps = mPreferences.getString("appname", "-1/");
				Global.logOut("test", "selectedApps : " + selectedApps);
				targetAppUidList = new ArrayList<Integer>();
				int pos = 0;
				int end;
				while ((end = selectedApps.indexOf("/", pos)) >= 0) {
					targetAppUidList.add(Integer.parseInt(selectedApps.substring(pos, end)));
					pos = end + 1;
				}

				// Remove if it has a UID less than 10000
				for (int i = 0; i < appInfoList.size();) {
					if (appInfoList.get(i).uid < 10000)
						appInfoList.remove(i);
					else
						i++;
				}

				Collections.sort(appInfoList, new NewComparator());

				// Remove if it has the same UID with others
				for (int i = 1; i < appInfoList.size();) {
					if (appInfoList.get(i).uid == appInfoList.get(i - 1).uid)
						appInfoList.remove(i);
					else
						i++;
				}


				int b = 0;
				for (int i = 0; i < appInfoList.size(); i++) {
					List<String> test = new ArrayList<>();
					test.add(appInfoList.get(i).packageName);
//					test.add(apkNm_List.get(i));
					List<String> duplicationList = new ArrayList<>(apkNmList);

					duplicationList.retainAll(test);

					if (!duplicationList.isEmpty()) {

						uidListSeq.add(Integer.parseInt(hashMap.get(appInfoList.get(i).packageName)));
						uidList.add(appInfoList.get(i).uid);

						Global.logOut(Global.LOG_TAG, "duplication count : " + i +", packageName : "+ duplicationList +"\n" +
								"uid : " + appInfoList.get(i).uid + ", user_app_seq : " + uidListSeq.get(b));
						b++;
					}
				}

			}catch (Exception e){
				e.printStackTrace();
			}

			return body;
		}

		@Override
		protected void onPostExecute(String s) {
			// super.onPostExecute(result);
			UmLoggerOverlayViewVisivility(1);
			Global.logOut("onPostExecute result : " + s);
			JSONObject jsonObject;

			try{
				jsonObject = new JSONObject(s);
				JSONArray jsonArray = new JSONArray(jsonObject.getString("Data"));
				List<String> list = new ArrayList<String>();
				proNmList = new ArrayList<String>();
				List<String> userAppSeqList = new ArrayList<String>();

				if(jsonObject != null && !jsonObject.equals("")) {

					String state = jsonObject.getString("state");

					JSONObject jsonObject1;
					for (int k = 0; k < jsonArray.length(); k++) {
						jsonObject1 = new JSONObject(jsonArray.getString(k));
						list.add(k, jsonObject1.getString("apk_nm"));
						proNmList.add(k, jsonObject1.getString("project_nm"));
						userAppSeqList.add(jsonObject1.getString("user_app_seq"));
						com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).setUserSeq(jsonObject1.getString("user_seq"));
					}
				}

				SystemInfo sysInfo = SystemInfo.getInstance();
				int i = 0;
				int j = appInfoList.size();
				List<String> dialogList  = new ArrayList<String>();
				while (true) {
					if (i >= j) {
						////
						CharSequence items[] = new CharSequence[localArrayList.size()];

						if (items.length <= 0){
							Handler mHandler = new Handler(Looper.getMainLooper());
							mHandler.postDelayed(new Runnable() {
								@Override
								public void run() {Toast.makeText(context, "Available test application does not exist. " +
										"Please ask your server administrator!!", Toast.LENGTH_LONG).show();}
							}, 0);
							Intent intent = new Intent(SelectApp.this, IntroActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
//							progressDialog.dismiss();
						}else{
							for (int d=0; d<localArrayList.size(); d++){items[d] = dialogList.get(d);}
							String text = "Select apps [+" + localArrayList.size() +"]";

							final ListDialog.Builder builder = new ListDialog.Builder(context)
									.setTitle(text)
									.setListItems(items, -1)
									.setCancelable(false)
									.setPositiveButton("START", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											int checkedId = ((ListDialog) dialog).getSelectedItemPostion();
//										CharSequence 앱명 = ((ListDialog)dialog).getListItem(checkedId);
											Global.logOut(Global.LOG_TAG, "position : "+checkedId + ", which : " + which);
											if (checkedId <= -1){
												Toast.makeText(context, "Please select the test app.", Toast.LENGTH_SHORT).show();
											}else {

												targetAppUidList.clear();
												targetAppUidList.add(uidList.get(checkedId));

												Global.logOut(Global.LOG_TAG, "uidListSeq : " + uidListSeq.get(checkedId) + "\n" +
														"package Name : " + pm.getNameForUid(uidList.get(checkedId)) + "");
												com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).setPackageNm(pm.getNameForUid(uidList.get(checkedId)));
												com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).setUserAppSeq(String.valueOf(uidListSeq.get(checkedId)));
//

												if (targetAppUidList.size() == 1) {
													isOneApp = true;
												} else {
													isOneApp = false;
												}

												SharedPreferences.Editor localEditor = mPreferences.edit();

												StringBuilder resultStr = new StringBuilder();
												for (int idx = 0; idx < targetAppUidList.size(); idx++) {
													resultStr.append(targetAppUidList.get(idx).toString()).append("/");
												}
												//
												getExeDeviceSeq();
												//
												localEditor.putString("appname", resultStr.toString());
												localEditor.putBoolean("one_app", isOneApp);
												localEditor.commit();
												Global.logOut("SelectApp", "brand Name : " + localArrayList.get(checkedId).toString());
//												Toast.makeText(context, "Project Setting > "+localArrayList.get(checkedId).toString()+" < Success!!!", Toast.LENGTH_LONG).show();
												com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).setBrandNm(localArrayList.get(checkedId).toString());
												Intent localIntent = new Intent();
												setResult(RESULT_FIRST_USER, localIntent);

												if (conn == null) {
													Toast.makeText(SelectApp.this, "모니터링을 시작하지 못하였습니다.", Toast.LENGTH_SHORT).show();
												} else {
													Calendar calendar = Calendar.getInstance();
													SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
													SimpleDateFormat dateFormatStr = new SimpleDateFormat("yyyyMMddHHmmss");

													String start_date = String.format("%s", dateFormat.format(calendar.getTime()));
													String start_dateStr = String.format("%s", dateFormatStr.format(calendar.getTime()));
													com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).setStartDate(start_date);
													com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).setStartStr(start_dateStr);

//													serviceIntent.putExtra("serviceIntent", "start");
													startService(serviceIntent);
													mPreferences.edit().putBoolean("running", true).commit();
													Intent intent = context.getPackageManager().getLaunchIntentForPackage(com.pion.pro.eNDer.util.PreferenceManager.getInstance(context).getPackageNm());
//													intent.addCategory(Intent.CATEGORY_HOME);
//													intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
													atamEnderInsert();
													startActivity(intent);
													moveTaskToBack(true);
													finish();

													//
												}

												dialog.dismiss();
												if (dfDialog != null) dfDialog.dismiss();
											}

										}
									});
							dfDialog = builder.create();
							dfDialog.show();
//							progressDialog.dismiss();

							break;
						}

					}

					List<String> packageNameList = new ArrayList<>();
					packageNameList.add(appInfoList.get(i).packageName);

					List<String> duplicationList = new ArrayList<>(list);
					duplicationList.retainAll(packageNameList);

					if(!duplicationList.isEmpty()) {
						localArrayList.add(sysInfo.getUidName(appInfoList.get(i).uid, pm));
						dialogList.add(sysInfo.getUidName(appInfoList.get(i).uid, pm));
					}

					i++;
				}
				Global.logOut(Global.LOG_TAG, "targetAppUidList1 : " + targetAppUidList + ", " + localArrayList.size());

			}catch (Exception e){
				e.printStackTrace();
			}finally {
				progressDialog.dismiss();
				UmLoggerOverlayViewVisivility(1);
			}

		}

		@Override
		protected void onCancelled() {
			super.onCancelled();

		}

	}

	public class CounterServiceConnection implements ServiceConnection {
		public void onServiceConnected(ComponentName className, IBinder boundService) {
			counterService = ICounterService.Stub.asInterface((IBinder) boundService);
			UmLoggerOverlayViewVisivility(2);
//			counterService = null;
//			stopService(serviceIntent);
//			getApplicationContext().unbindService(conn);
//			getApplicationContext().bindService(serviceIntent, conn, 0);
////			Toast.makeText(SelectApp.this, "Service STOP!!", Toast.LENGTH_SHORT).show();
		}

		public void onServiceDisconnected(ComponentName className) {
			counterService = null;
			getApplicationContext().unbindService(conn);
			getApplicationContext().bindService(serviceIntent, conn, 0);
//			Toast.makeText(SelectApp.this, "Monitoring STOP", Toast.LENGTH_SHORT).show();

		}
	}


}