package com.pion.pro.eNDer.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pion.pro.eNDer.R;
import com.pion.pro.eNDer.service.UMLoggerService;
import com.pion.pro.eNDer.util.EnderImgsData;
import com.pion.pro.eNDer.util.Global;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import static com.pion.pro.eNDer.R.id.pager;


public class ReportApp extends Activity {

    protected static final String TAG = ReportApp.class.getSimpleName().toString();
    private View v;
    PopupWindow popupWindow;
    View pop_View;
    LinearLayout linear;
    Button btn_report_back;
    Context mContext;
    Handler overlayHandler;

    ////////////////////////////////
    private int COUNT = 0;				//아이템 갯수
    private int mPrevPosition;			//이전에 선택되었던 포지션 값

    private ViewPager mPager;			//뷰 페이저
//    private LinearLayout mPageMark;		//현재 몇 페이지 인지 나타내는 뷰

    //아이템의 배경화면 색상. 아이템을 구분하기 위해.
    //private int[] mColorArray = {Color.YELLOW, Color.BLUE, Color.CYAN, Color.DKGRAY, Color.GRAY};
//    private BitmapDrawable[] mColorArray;
//    private String[] editPop, resStr,brand_nm, uriFileName;
    //////////////////////////
    List<EnderImgsData.ImageData> lcs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = ReportApp.this;
        Global.IsSingPadActivity = true;

        UmLoggerOverlayViewVisivility(1);

        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        String enderImgsData = mPreferences.getString("enderImgsData", "");
        if ( enderImgsData.equals("") ) {
            Toast.makeText(mContext, "Image File Not Found !!! " , Toast.LENGTH_SHORT).show();
            try {
                Thread.sleep(2000);
                finish();
            } catch (Exception e){}

        }

        Log.i(TAG,"======== enderImgsData : " + enderImgsData );
        Type collectionType = new TypeToken<List<EnderImgsData.ImageData>>(){}.getType();
        lcs = (List<EnderImgsData.ImageData>) new Gson().fromJson( enderImgsData , collectionType);
        COUNT = lcs.size();
        Log.i(TAG,"======== imgCounter : " + COUNT );

//        mColorArray = new BitmapDrawable[COUNT];
//        editPop = new String[COUNT];
//        resStr = new String[COUNT];
//        brand_nm = new String[COUNT];
//        uriFileName = new String[COUNT];

//        Bitmap bm = null;
//        int i = 0;
//        for (EnderImgsData.ImageData imgs : lcs) {
//
//            String imgPath = imgs.getUriFileName();
//
//            try {
//                if ( imgPathExist(imgs.getUriFileName()) ) {
//                    bm = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), getUriFromPath(imgPath));
//                } else {
//                    // 이미지가 삭제되었다면 대체 이미지로 바꿈
//                    bm = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.intro_1);
//                }
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            mColorArray[i] = new BitmapDrawable(getResources(), bm);
//            editPop[i] = imgs.getEditPop();
//            resStr[i] = imgs.getResStr();
//            brand_nm[i] = imgs.getBrand_nm();
//            uriFileName[i] = imgs.getUriFileName();
//
//            i++;
//        }




        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
            v = LayoutInflater.from(this).inflate(R.layout.report, null, true);
            setContentView(v);
            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

            btn_report_back = (Button) findViewById(R.id.btn_report_back);
            btn_report_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }else {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            v = LayoutInflater.from(this).inflate(R.layout.report, null, true);
            setContentView(v);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, 0);

//            btn_report_back = (Button) findViewById(R.id.btn_report_back);
//            btn_report_back.setText("<");
//            btn_report_back.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onPause();
//                }
//            });
        }


        ///////////////////////////===============================
//        mPageMark = (LinearLayout)findViewById(R.id.page_mark);			//상단의 현재 페이지 나타내는 뷰
        mPager = (ViewPager)findViewById(pager);						//뷰 페이저
        mPager.setAdapter(new BkPagerAdapter( getApplicationContext(), getLayoutInflater() ));//PagerAdapter로 설정
        mPager.setCurrentItem(COUNT);			//무한 스크롤 하기 위해서는 아이템을 3배로 가지고 있고 그 중 가운데 범위의 아이템만 보이게 한다
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {	//아이템이 변경되면, gallery나 listview의 onItemSelectedListener와 비슷
            @Override public void onPageSelected(int position) {
                if(position < COUNT)												//3배의 아이템중 앞쪽이면 뷰페이져의 포지션을 가운데 범위로 이동시킨다
                    mPager.setCurrentItem(position+COUNT, false);
                else if(position >= COUNT*2)										//3배의 아이템 중 뒤쪽이면 뷰페이져의 포지션을 가운데 범위로 이동시킨다
                    mPager.setCurrentItem(position - COUNT, false);
                else {																	//가운데 범위이면 상단의 페이지 표시를 변경한다
                    position -= COUNT;
                    //아이템이 선택이 되었으면
//                    mPageMark.getChildAt(mPrevPosition).setBackgroundResource(R.drawable.page_not);	//이전 페이지에 해당하는 페이지 표시 이미지 변경
//                    mPageMark.getChildAt(position).setBackgroundResource(R.drawable.page_select);		//현재 페이지에 해당하는 페이지 표시 이미지 변경
                    mPrevPosition = position;				//이전 포지션 값을 현재로 변경
                }
            }
            @Override public void onPageScrolled(int position, float positionOffest, int positionOffsetPixels) {}
            @Override public void onPageScrollStateChanged(int state) {}
        });

//        initPageMark();	//현재 페이지 표시하는 뷰 초기화
        ////////////////////////=================================

//        final CoverFlowView<MyCoverFlowAdapter> mCoverFlowView = (CoverFlowView<MyCoverFlowAdapter>) findViewById(R.id.coverflow);
//
//        // adapter memory check
//        final MyCoverFlowAdapter adapter = new MyCoverFlowAdapter(getApplicationContext());
//
//        mCoverFlowView.setAdapter(adapter);
//        mCoverFlowView
//                .setCoverFlowListener(new CoverFlowView.CoverFlowListener<MyCoverFlowAdapter>() {
//
//                    @Override
//                    public void imageOnTop(
//                            CoverFlowView<MyCoverFlowAdapter> view,
//                            int position, float left, float top, float right,
//                            float bottom) {
//                        Log.e(TAG, position + " on top !");
//
//                        String appName = adapter.appNameHash.get(position);
//                        String captureName = adapter.imgsStr.get(position);
//                        String appDesc = adapter.appDescHash.get(position);
//                        String pageNum = Integer.toString(position+1) + " / " + Integer.toString(adapter.getCount());
//                        //String captureDate = captureName.split("_")[1].replace(".png","");
//
//                        ((TextView)v.findViewById(R.id.imgName)).setText(appName);
//                        ((TextView)v.findViewById(R.id.imgDate)).setText(captureName);
//                        ((EditText)v.findViewById(R.id.appDesc)).setText(appDesc);
//                        ((TextView)v.findViewById(R.id.pageNum)).setText(pageNum);
//                        //Log.e(VIEW_LOG_TAG, "captureName : "  + captureName + " / " + captureDate );
//                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
//                            executeDelayed();
//                        }
//                    }
//
//                    @Override
//                    public void topImageClicked(CoverFlowView<MyCoverFlowAdapter> view, int position) {
//                        Log.e(TAG, position + " clicked!");
//
////                        IntentData data = new IntentData();
////                        data.setBitmap(adapter.imgsHash.get(position));
////                        Intent intent = new Intent(getApplicationContext(), ImageZoomActivity.class);
////                        intent.putExtra("OBJECT", data);
////                        startActivity(intent);
//
////                        popImage(adapter.imgsHash.get(position));
//                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
//                            executeDelayed();
//                        }
//                    }
//
//                    @Override
//                    public void invalidationCompleted() {
//
//                    }
//                });
//
//        mCoverFlowView
//                .setTopImageLongClickListener(new CoverFlowView.TopImageLongClickListener() {
//
//                    @Override
//                    public void onLongClick(int position) {
//                        Log.e(TAG, "top image long clicked == >"
//                                + position);
//                    }
//                });

//        findViewById(R.id.change_bitmap_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                adapter.changeBitmap();
//            }
//        });
    }

    public Uri getUriFromPath(String fileName) {

        // Uri : file:///storage/emulated/0/Pictures/Screenshots/Screenshot_2016-09-26-21-07-34.png
        //String path = Global.Wonder_IMG_PATH + "/" + fileName;
        String path = "file:///sdcard/Wonder/" + fileName;

        Log.i(TAG,"============================" + path );
        Uri fileUri = Uri.parse( path );

        return  fileUri;
    }

    private boolean imgPathExist(String imgPath) {

        Boolean rtn = false;
        // Uri : file:///storage/emulated/0/Pictures/Screenshots/Screenshot_2016-09-26-21-07-34.png
        String path = Global.Wonder_IMG_PATH + "/" + imgPath;

        File files = new File(path);
        if ( files.exists() == true ) {
            rtn = true;
        }

        Log.i(TAG,"======== file path =========================" + path + " / exist = " + rtn );
        return rtn;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Global.CaptureYn = false;
        UmLoggerOverlayViewVisivility(1);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            executeDelayed();
        }
        if (UMLoggerService.oView.getVisibility() == View.VISIBLE){
            UmLoggerOverlayViewVisivility(1);
        }
    }

    private void UmLoggerOverlayViewVisivility(final int visivility){
        overlayHandler = new Handler();
        overlayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(visivility == 0){
                    if (UMLoggerService.oView != null){
                        UMLoggerService.oView.setVisibility(View.VISIBLE);
                        UMLoggerService.oView.setEnabled(true);
                    }
                }else if (visivility == 1){
                    if (UMLoggerService.oView != null){
                        UMLoggerService.oView.setVisibility(View.INVISIBLE);
                        UMLoggerService.oView.setEnabled(false);
                    }
                }else if (visivility == 2){
                    UMLoggerService.oView.setVisibility(View.INVISIBLE);
                    UMLoggerService.oView.setEnabled(false);
                }
            }
        }, 2000);

    }
    private void popImage(Bitmap bitmap) {

//        AlertDialog.Builder alert = new AlertDialog.Builder(this);
//        alert.setTitle("IMAGE VIEWER");
//
//        // Set an EditText view to get user input
//        final ImageView imageview = new ImageView(this);
//
//        LinearLayout.LayoutParams lparam = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
//
//        lparam.width = 1000;
//        lparam.height = 1200;
//
//        imageview.setLayoutParams(lparam);
//        BitmapDrawable background = new BitmapDrawable(bitmap);
//        imageview.setImageDrawable(background);
//
//        alert.setView(imageview);
//        alert.setIcon(R.drawable.icon);
//
//
//        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//
//            }
//        });
//
//        alert.show();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        Resources lang_res = getResources();
        DisplayMetrics lang_dm = lang_res.getDisplayMetrics();
        int lang_width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 350, lang_dm);
        int lang_height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 710, lang_dm);
        linear = (LinearLayout) findViewById(R.id.reportLayout);

        pop_View = View.inflate(this, R.layout.activity_image_zoom, null);
        popupWindow = new PopupWindow(pop_View, lang_width, lang_height, true);
//        popupWindow.setAnimationStyle(-1);
        popupWindow.showAtLocation(linear, Gravity.FILL_VERTICAL, 0, 0);

        // 팝업 닫기 버튼
        ImageView close_Btn = (ImageView) pop_View.findViewById(R.id.imgView);
        BitmapDrawable background = new BitmapDrawable(bitmap);
        close_Btn.setImageDrawable(background);
        close_Btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    executeDelayed();
                }
                popupWindow.dismiss(); // 팝업 닫기
            }
        });

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(data.getStringExtra("serviceIntent").equals("stop")){
//            stopService(data);
//        }
//    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
////        Intent serviceIntent = new Intent(ReportApp.this, UMLoggerService.class);
////        serviceIntent.putExtra("serviceIntent", "start");
////        startService(serviceIntent);
//        onPause();
//    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Global.IsSingPadActivity = false;
        UmLoggerOverlayViewVisivility(0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        Global.IsSingPadActivity = false;
        UmLoggerOverlayViewVisivility(1);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        Global.IsSingPadActivity = false;
        UmLoggerOverlayViewVisivility(0);
        onDestroy();
//        moveTaskToBack(true);
//        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        Global.IsSingPadActivity = false;
//        moveTaskToBack(true);
        UmLoggerOverlayViewVisivility(0);

    }
    private boolean actFlag = false;
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        Global.logOut(TAG, "Home Button Touch");
        UmLoggerOverlayViewVisivility(1);
        Toast.makeText(mContext, "We are currently testing. It is not recommended to leave the screen.", Toast.LENGTH_LONG).show();
        Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm());
        startActivity(intent);
        moveTaskToBack(true);
        finish();
    }

    //hide Navigation Bar
    public void executeDelayed() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideNavBar();
            }
        }, 0);
    }

    public void hideNavBar() {
        View v = ((Activity)mContext).getWindow().getDecorView();
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }


    //상단의 현재 페이지 표시하는 뷰 초기화
    private void initPageMark(){
        for(int i=0; i<COUNT; i++)
        {
            ImageView iv = new ImageView(getApplicationContext());	//페이지 표시 이미지 뷰 생성
            iv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            //첫 페이지 표시 이미지 이면 선택된 이미지로
            if(i==0)
                iv.setBackgroundResource(R.drawable.page_select);
            else	//나머지는 선택안된 이미지로
                iv.setBackgroundResource(R.drawable.page_not);

            //LinearLayout에 추가
//            mPageMark.addView(iv);
        }
        mPrevPosition = 0;	//이전 포지션 값 초기화
    }

    //Pager 아답터 구현
    private class BkPagerAdapter extends PagerAdapter{

        private Context mContext;
        LayoutInflater inflater;

        public BkPagerAdapter( Context con, LayoutInflater inflater ) {
            super();
            mContext = con;

            //전달 받은 LayoutInflater를 멤버변수로 전달
            this.inflater = inflater;
        }

        @Override
        public int getCount() { return COUNT * 3; }

        //뷰페이저에서 사용할 뷰객체 생성/등록
        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {

            View view = null;

            //새로운 View 객체를 Layoutinflater를 이용해서 생성
//            view = inflater.inflate(R.layout.viewpager_childview, null);
            view = LayoutInflater.from(mContext).inflate(R.layout.viewpager_childview, null, true);

            container.addView(getImageData(view, position),0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // TODO Auto-generated method stub

            //ViewPager에서 보이지 않는 View는 제거
            //세번째 파라미터가 View 객체 이지만 데이터 타입이 Object여서 형변환 실시
            container.removeView((View)object);

        }

        // instantiateItem메소드에서 생성한 객체를 이용할 것인지
        @Override public boolean isViewFromObject(View view, Object obj) { return view == obj; }

        @Override public void finishUpdate(View arg0) {}
        @Override public void restoreState(Parcelable arg0, ClassLoader arg1) {}
        @Override public Parcelable saveState() { return null; }
        @Override public void startUpdate(View arg0) {}
    }

    private View getImageData(View view, int position) {

        Bitmap bm = null;

        Log.d(TAG, "COUNT : " + COUNT + ", position : " + position);
        position %= COUNT;

        String imgPath = lcs.get(position).getUriFileName();

        if ( imgPathExist(imgPath) ) {
            try {
                bm = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), getUriFromPath(imgPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // 이미지가 삭제되었다면 대체 이미지로 바꿈
            bm = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.intro_1);
        }

        ((TextView)view.findViewById(R.id.imgName)).setText(lcs.get(position).getBrand_nm());
        ((TextView)view.findViewById(R.id.imgDate)).setText(lcs.get(position).getUriFileName());

        ((ImageView)view.findViewById(R.id.img_viewpager_childimage)).setImageBitmap(bm);
        ((EditText)view.findViewById(R.id.appDesc)).setText("(CPU:MEM:Battery)\n" + lcs.get(position).getResStr() +"\n"+ lcs.get(position).getEditPop() );

        String pageNum = Integer.toString(position+1) + " / " + Integer.toString(COUNT);
        ((TextView)view.findViewById(R.id.pageNum)).setText(pageNum);

        return view;
    }

}