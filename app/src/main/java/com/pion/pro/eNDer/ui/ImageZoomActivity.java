package com.pion.pro.eNDer.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.pion.pro.eNDer.R;
import com.pion.pro.eNDer.util.IntentData;

/**
 * Created by joy on 2016. 10. 5..
 */

public class ImageZoomActivity extends Activity {

    protected static final String VIEW_LOG_TAG = "ImageZoomActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_image_zoom);

        Intent intent = getIntent();
        IntentData data = (IntentData) intent.getSerializableExtra("OBJECT");

        LinearLayout zoomImg = (LinearLayout)findViewById(R.id.zoomLinear);

        BitmapDrawable background = new BitmapDrawable(data.getBitmap());
        zoomImg.setBackgroundDrawable(background);

        zoomImg.setOnClickListener(serviceStartButtonListener);
    }

    private Button.OnClickListener serviceStartButtonListener = new LinearLayout.OnClickListener() {
        public void onClick(View v) {
            finish();
        }
    };
}