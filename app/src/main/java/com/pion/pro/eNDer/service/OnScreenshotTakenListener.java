package com.pion.pro.eNDer.service;

/**
 * Created by joy on 2016. 9. 20..
 */

import android.net.Uri;

public interface OnScreenshotTakenListener {
    void onScreenshotTaken(Uri uri);
}