package com.pion.pro.eNDer.util;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by joy on 2016. 10. 5..
 */

public class IntentData implements Serializable {

    private static final long serialVersionUID = 1L;

    private Bitmap bmp;

    public Bitmap getBitmap(){
        return bmp;
    }

    public void setBitmap(Bitmap bmp){
        this.bmp = bmp;
    }

}