///*
//Copyright (C) 2011 The University of Michigan
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//Please send inquiries to powertutor@umich.edu
// */
//
//package com.pion.pro.eNDer.ui;
//
//import java.io.File;
//import java.io.FilenameFilter;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.ComponentName;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.ServiceConnection;
//import android.content.SharedPreferences;
//import android.content.pm.ApplicationInfo;
//import android.content.pm.PackageManager;
//import android.graphics.Point;
//import android.hardware.Sensor;
//import android.hardware.SensorEvent;
//import android.hardware.SensorEventListener;
//import android.hardware.SensorManager;
//import android.media.MediaScannerConnection;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Environment;
//import android.os.Handler;
//import android.os.IBinder;
//import android.os.Message;
//import android.os.Process;
//import android.os.Vibrator;
//import android.preference.PreferenceManager;
//import android.telephony.TelephonyManager;
//import android.util.Log;
//import android.view.Display;
//import android.view.Gravity;
//import android.view.KeyEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.pion.pro.eNDer.R;
//import com.pion.pro.eNDer.apiService.PackageInfo;
//import com.pion.pro.eNDer.apiService.ProjectListVO;
//import com.pion.pro.eNDer.apiService.ServiceApiHttpClient;
//import com.pion.pro.eNDer.components.TrafficUids;
//import com.pion.pro.eNDer.service.ICounterService;
//import com.pion.pro.eNDer.service.ProcessManager;
//import com.pion.pro.eNDer.service.Tools;
//import com.pion.pro.eNDer.service.UMLoggerService;
//import com.pion.pro.eNDer.ui.view.ListDialog;
//import com.pion.pro.eNDer.util.ApiUrl;
//import com.pion.pro.eNDer.util.Global;
//import com.pion.pro.eNDer.util.SystemInfo;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
///** The main view activity for PowerTutor */
//public class UMLogger extends Activity {
//	private static final String TAG = "UMLogger";
//
//	public static final String CURRENT_VERSION = "1.2"; // Don't change this...
//
//	private SharedPreferences mPreferences;
//	public Intent serviceIntent;
//	private ICounterService counterService;
//	private CounterServiceConnection conn;
//	private Context mContext;
//
//	private Button serviceStartButton;
//	private Button selectAppButton;
//	private Button menu2;
//	private Button preferenceButton;
//	private TextView modifiedBy;
//
//	private String[] componentNames;
//	private int noUidMask;
//
//	private TextView appLabel;
//	private TextView titleText;
//	public static final int REQUEST_CODE_APP = 1727;
//	public static final int REQUEST_CODE_OPTION = 1728;
//	public static final int DIALOG_HELP = 0;
//	private List<Integer> targetAppUidList;
////	private long hz;
//
//	private int versionClickCount;
//	private long versionClickTime = 0;
//	DeviceRePlaceTask deviceRePlaceTask = null;
//
//	String imei;		//디바이스 고유번호
//	String device_name;	//디바이스명
//	String cd_name;		//제조사명
//	int width;			//x 축
//	int height;			//y 축
//	String os_ver; 		//OS Version
//	Intent loginGetIntent;		//Login에서 받은 인텐트 값
//	static String user_seq;		//User Seq
//	private LinearLayout end_icon;	//X
//	PackageManager packageManager;
//	List<String> list2;
//	List<String> list3;
//	public static Map<String, String> hashMap;
//	ArrayList<String> localArrayList;
//	List<Integer> uidList;
//	List<ApplicationInfo> appInfoList;
//	List<Integer> uidList_seq = new ArrayList<Integer>();
//	private ListDialog sadialog;
//	List<String> dialogList;
//	List<String> proNmList;
//
//	/** Called when the activity is first created. */
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//		mContext = getApplicationContext();
//
//		loginGetIntent = getIntent();
//		Log.d(Global.LOG_TAG, "UMLogger LogingetIntent Extra String NAME : " + loginGetIntent.getStringExtra("user_name") + ", SEQ : " + loginGetIntent.getStringExtra("user_seq")
//				+ ", " + Build.MODEL + ", " + Build.VERSION.RELEASE.substring(0, 3).toString());
//		user_seq = loginGetIntent.getStringExtra("user_seq");
//
//		//get Deivice IMEI NUMBER
//		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//		imei = telephonyManager.getDeviceId();
//		device_name = Build.MODEL;
//		cd_name = Build.MANUFACTURER;
//		Display display = getWindowManager().getDefaultDisplay();
//		Point size = new Point();
//		display.getSize(size);
//		width = size.x;
//		height = size.y;
//		os_ver = Build.VERSION.RELEASE;
//		//공유 변수에 저장 (로그데이터에 쌓을 목적으로 저장함)
//		com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setImei(imei);
//
//		/////////////////////////////
//
//		//set Device IMEI NUMBER
////		if (!com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).isLogin()){
//		deviceRePlaceTask = new DeviceRePlaceTask(imei, device_name);
//		HashMap params = new HashMap();
//		params.put("imei", imei);
//		params.put("device_nm", device_name);
//		params.put("cd_desc", cd_name.toUpperCase().toString());
//		params.put("size_x", String.valueOf(width));
//		params.put("size_y", String.valueOf(height));
//		params.put("os_ver", os_ver.substring(0, 3).toString());
//
//		deviceRePlaceTask.execute(params);
//
////		}
////		getAppPackageListStr();	//패키지 리스트 리퀘스트 요청
//
//
//
//		serviceIntent = new Intent(this, UMLoggerService.class);
//		conn = new CounterServiceConnection();
//
//
//		versionClickCount = 0;
//
//		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
//		setContentView(R.layout.ender_main);
//		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);
//
//
//		packageManager = getPackageManager();
//
//
//		end_icon = (LinearLayout)findViewById(R.id.end_icon);
//		end_icon.setOnClickListener(selectAppConnect);
//		ArrayAdapter<?> adapterxaxis = ArrayAdapter.createFromResource(this,
//				R.array.xaxis, android.R.layout.simple_spinner_item);
//		adapterxaxis.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
////		appLabel = (TextView) findViewById(R.id.s0);
//		serviceStartButton = (Button) findViewById(R.id.servicestartbutton);
//		//selectAppButton = (Button) findViewById(R.id.selectappbutton);
//		menu2 = (Button) findViewById(R.id.menu2);
//		preferenceButton = (Button) findViewById(R.id.preferencebutton);
//		modifiedBy = (TextView) findViewById(R.id.modifiedby);
//
////		appLabel.setOnClickListener(hiddenHelpListener);
//		serviceStartButton.setOnClickListener(serviceStartButtonListener);
//		//selectAppButton.setOnClickListener(selectAppButtonListener);
//		menu2.setOnClickListener(selectReportButtonListener);
//		preferenceButton.setOnClickListener(preferenceButtonListener);
//		modifiedBy.setOnClickListener(hiddenGCListener);
//
//		if (savedInstanceState != null) {
//			componentNames = savedInstanceState.getStringArray("componentNames");
//			noUidMask = savedInstanceState.getInt("noUidMask");
//		}
//
//
//		if (counterService != null) {
////			serviceStartButton.setText("STP");//모니터링 정지
//			serviceStartButton.setBackgroundResource(R.drawable.spider_stop);
//		} else {
////			serviceStartButton.setText("Start");//모니터링 시작
//			serviceStartButton.setBackgroundResource(R.drawable.spider_start);
//			String appName = this.mPreferences.getString("appname", "-1/");
//
//			targetAppUidList = new ArrayList<Integer>();
//			int pos = 0;
//			int end;
//			while ((end = appName.indexOf("/", pos)) >=0 ) {
//				targetAppUidList.add(Integer.parseInt(appName.substring(pos, end)));
//				pos = end + 1;
//			}
//
//			if (targetAppUidList.contains(-1)) {
////				appName = "Foreground App";
//				appName = "Select App";
//			} else {
//				SystemInfo sysInfo = SystemInfo.getInstance();
//				PackageManager pm = getPackageManager();
//
//				appName = sysInfo.getUidName(targetAppUidList.get(0), pm);
//				//공유변수 설정 로그데이터에 쌓을 목적으로 저장함
//				com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setBrandNm(appName);
//				if (targetAppUidList.size() > 1) {
//					appName = appName + String.format(" 외 %d개", targetAppUidList.size()-1);
//				}
//			}
//
//			this.titleText = (TextView) findViewById(R.id.s2);
//			this.titleText.setText(appName);
//
//
//			if (componentNames == null)
//				componentNames = new String[]{"LCD", "CPU", "WiFi", "3G"};
//
//			// Not Running, after Restart the App, All Components set Checked
//			int ignMask = mPreferences.getInt("topIgnoreMask", 0);
//			if (!mPreferences.getBoolean("running", false)) {
//				ignMask = 0;
//				mPreferences.edit().putInt("topIgnoreMask", ignMask).commit();
//			}
//			for (int i = 0; i < componentNames.length; i++) {
//				if ((noUidMask & 1 << i) != 0)
//					continue;
//				final TextView filterToggle1 = new TextView(UMLogger.this);
//				final int index = i;
//				filterToggle1.setText(componentNames[i]);
//				filterToggle1.setGravity(Gravity.CENTER);
//
//				filterToggle1.setTextColor((ignMask & 1 << index) == 0 ? 0xFFFFFFFF : 0xFF888888);
//
//				filterToggle1.setFocusable(true);
//				filterToggle1.setOnClickListener(new View.OnClickListener() {
//					public void onClick(View v) {
//						int ignMask = mPreferences.getInt("topIgnoreMask", 0);
//						if ((ignMask & 1 << index) == 0) {
//							mPreferences.edit().putInt("topIgnoreMask", ignMask | 1 << index).commit();
//							filterToggle1.setTextColor(0xFF888888);
//						} else {
//							mPreferences.edit().putInt("topIgnoreMask", ignMask & ~(1 << index)).commit();
//							filterToggle1.setTextColor(0xFFFFFFFF);
//						}
//					}
//				});
//
//			}
//
//		}
//
//		// NDK Test
////		hz = hzFromJNI();
////		mPreferences.edit().putLong("hz", hz).commit();
//	}
//
//	// NDK
////	static {
////		System.loadLibrary("clibrary");
////	}
////
////	private native long hzFromJNI();
//
//
//
//	@Override
//	public void onResume() {
//		super.onResume();
//		// Sanghak's comment :
//		// This is because of the issue which is about printing error log below, so make some delay
//		// error log : Performing stop of activity that is not resumed
//		Handler handler = new Handler();
//		handler.postDelayed(new Runnable() {
//			@Override
//			public void run() {
//				getApplicationContext().bindService(serviceIntent, conn, 0);
//			}
//		}, 250);
////		getApplicationContext().bindService(serviceIntent, conn, 0);
//
//	}
//
//
//	@Override
//	public void onPause() {
//		super.onPause();
//
//		if (conn != null){
//			getApplicationContext().unbindService(conn);
//		}
//
//	}
//
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		if (requestCode == REQUEST_CODE_APP) {
//			this.mPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//			String appName = this.mPreferences.getString("appname", "-1/");
//
//			targetAppUidList = new ArrayList<Integer>();
//			int pos = 0;
//			int end;
//			while ((end = appName.indexOf("/", pos)) >=0 ) {
//				targetAppUidList.add(Integer.parseInt(appName.substring(pos, end)));
//				pos = end + 1;
//			}
//
//			if (targetAppUidList.contains(-1)) {
//				appName = "Select App";
//			} else {
//				SystemInfo sysInfo = SystemInfo.getInstance();
//				PackageManager pm = getPackageManager();
//
//				appName = sysInfo.getUidName(targetAppUidList.get(0), pm);
//				if (targetAppUidList.size() > 1) {
//					appName = appName + String.format(" 외 %d개", targetAppUidList.size()-1);
//				}
//			}
//
//			this.titleText.setText(appName);
//
//	//선택앱에서 선택하고 바로 시작작 두번째
//			// 서비스 스타트 버튼 클릭시 기능임..
//			/*if (counterService != null) {
//				mPreferences.edit().putBoolean("running", false).commit();
//				Calendar calendar = Calendar.getInstance();
//				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//				String end_date = String.format("%s", dateFormat.format(calendar.getTime()));
//				com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setEnderSeq(end_date);
//				serviceStartButton.setBackgroundResource(R.drawable.start);
//				stopService(serviceIntent);
//				// TODO : Media Scan
//				MediaScanFile.getInstance(mContext, null);
//			} else {
//				if (conn == null) {
//					Toast.makeText(UMLogger.this, "모니터링을 시작하지 못하였습니다.", Toast.LENGTH_SHORT).show();
//				} else {
//					Calendar calendar = Calendar.getInstance();
//					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//					SimpleDateFormat dateFormatStr = new SimpleDateFormat("yyyyMMddHHmmss");
//
//					String start_date = String.format("%s", dateFormat.format(calendar.getTime()));
//					String start_dateStr = String.format("%s", dateFormatStr.format(calendar.getTime()));
//					com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setStartDate(start_date);
//					com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setStartStr(start_dateStr);
//
//					serviceStartButton.setBackgroundResource(R.drawable.start_1);
//					mPreferences.edit().putBoolean("running", true).commit();
//
//					startService(serviceIntent);
//
//				}
//			}*/
//		}
//	}
//
//	@Override
//	protected void onSaveInstanceState(Bundle outState) {
//		super.onSaveInstanceState(outState);
//		outState.putStringArray("componentNames", componentNames);
//		outState.putInt("noUidMask", noUidMask);
//	}
//
//
//	private LinearLayout.OnClickListener selectAppConnect = new View.OnClickListener() {
//		@Override
//		public void onClick(View v) {
//
//			Intent intent = new Intent(v.getContext(), SelectApp.class);
//			intent.putExtra("user_seq", user_seq);
//			startActivityForResult(intent, REQUEST_CODE_APP);
//
//		}
//	};
//
//	private Button.OnClickListener serviceStartButtonListener = new Button.OnClickListener() {
//		public void onClick(final View v) {
////			serviceStartButton.setEnabled(false);
//			if (targetAppUidList.contains(-1)) {
//				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//						UMLogger.this);
//
//				// 제목셋팅
//				alertDialogBuilder.setTitle("앱 선택 알림");
//
//				// key 셋팅
//				alertDialogBuilder
//						.setOnKeyListener(new DialogInterface.OnKeyListener() {
//							public boolean onKey(DialogInterface dialog,
//												 int keyCode, KeyEvent event) {
//								if (keyCode == KeyEvent.KEYCODE_BACK) {
//									finish();
//									dialog.dismiss();
//									return true;
//								}
//								return false;
//							}
//						});
//
//				// AlertDialog 셋팅
//				alertDialogBuilder
//						.setMessage("선택되어진 앱이 없습니다. 테스트 시작 전 테스트할 앱을 선택해주세요. 앱 선택을 하시겠습니까?")
//						.setCancelable(false)
//						.setPositiveButton("OK",
//								new DialogInterface.OnClickListener() {
//									public void onClick(
//											DialogInterface dialog, int id) {
//										Intent intent = new Intent(v.getContext(), SelectApp.class);
//										intent.putExtra("user_seq", user_seq);
//										startActivityForResult(intent, REQUEST_CODE_APP);
//									}
//								})
//						.setNegativeButton("Cancel",
//								new DialogInterface.OnClickListener() {
//									public void onClick(
//											DialogInterface dialog, int id) {
//										// 다이얼로그를 취소한다
//										dialog.cancel();
//									}
//								});
//
//				// 다이얼로그 생성
//				AlertDialog alertDialog = alertDialogBuilder.create();
//				alertDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
//
//				// 다이얼로그 보여주기
//				alertDialog.show();
//
//			}else{
//				if (counterService != null) {
//					mPreferences.edit().putBoolean("running", false).commit();
//					Calendar calendar = Calendar.getInstance();
//					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//					String end_date = String.format("%s", dateFormat.format(calendar.getTime()));
//					com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setEnderSeq(end_date);
//					serviceStartButton.setBackgroundResource(R.drawable.spider_start);
//
//					stopService(serviceIntent);
//					// TODO : Media Scan
//					MediaScanFile.getInstance(mContext, null);
//				} else {
//					if (conn == null) {
//						Toast.makeText(UMLogger.this, "모니터링을 시작하지 못하였습니다.", Toast.LENGTH_SHORT).show();
//					} else {
//						Calendar calendar = Calendar.getInstance();
//						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//						SimpleDateFormat dateFormatStr = new SimpleDateFormat("yyyyMMddHHmmss");
//
//						String start_date = String.format("%s", dateFormat.format(calendar.getTime()));
//						String start_dateStr = String.format("%s", dateFormatStr.format(calendar.getTime()));
//						com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setStartDate(start_date);
//						com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setStartStr(start_dateStr);
//
//						serviceStartButton.setBackgroundResource(R.drawable.spider_stop);
//
//						startService(serviceIntent);
//						mPreferences.edit().putBoolean("running", true).commit();
//						Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).getPackageNm());
//        				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_FROM_BACKGROUND);
//        				startActivity(intent);
//						finish();
//
//						//
//					}
//				}
//			}
//
//
//
//		}
//	};
//
////	//선택앱에서 선택하고 바로 시작작 첫번째
////		private Button.OnClickListener serviceStartButtonListener = new Button.OnClickListener() {
////		public void onClick(View v) {
////			serviceStartButton.setEnabled(mPreferences.getBoolean("running", false));
////
////			Intent intent = new Intent(v.getContext(), SelectApp.class);
////			intent.putExtra("user_seq", user_seq);
////			startActivityForResult(intent, REQUEST_CODE_APP);
////
////		}
////	};
//
//////	 앱 선택은 하지 않고 포그라운드 앱을 기본적으로 테스트 한다
////	private View.OnClickListener selectAppButtonListener = new View.OnClickListener() {
////		public void onClick(View v) {
////			Intent intent = new Intent(v.getContext(), SelectApp.class);
////			startActivityForResult(intent, REQUEST_CODE_APP);
////		}
////	};
//
//	private View.OnClickListener selectReportButtonListener = new View.OnClickListener() {
//		public void onClick(View v) {
//			Intent intent = new Intent(v.getContext(), ReportApp.class);
////			startActivityForResult(intent, REQUEST_CODE_APP);
//			startActivity(intent);
//		}
//	};
//	private Button.OnClickListener preferenceButtonListener = new Button.OnClickListener() {
//		public void onClick(View v) {
////			Intent intent = new Intent(v.getContext(), EditPreferences.class);
//			Toast.makeText(UMLogger.this, "This feature is currently unavailable.", Toast.LENGTH_SHORT).show();
////			Intent intent = new Intent(v.getContext(), PrefActivity.class);
////	        startActivityForResult(intent, REQUEST_CODE_OPTION);
//
//		}
//	};
//
//	private View.OnClickListener hiddenHelpListener = new View.OnClickListener() {
//		@SuppressWarnings("deprecation")
//		@Override
//		public void onClick(View v) {
//			showDialog(DIALOG_HELP);
//		}
//	};
//
//	private View.OnClickListener hiddenGCListener = new View.OnClickListener() {
//		@Override
//		public void onClick(View v) {
//			long curTime = System.currentTimeMillis();
//
//			if (versionClickCount >= 5) {
//				versionClickCount = 0;
//				System.gc();
//				Toast.makeText(UMLogger.this, "Send GC Hint to VM", Toast.LENGTH_LONG).show();
//			} else if (curTime - versionClickTime < 3000)
//				versionClickCount++;
//			else {
//				versionClickCount = 0;
//				versionClickTime = curTime;
//			}
//		}
//	};
//
//	@Override
//	protected Dialog onCreateDialog(int id) {rrrrrrrr
//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
//		builder.setMessage(R.string.help_term)
//				.setCancelable(false)
//				.setTitle("도움말")
//				.setPositiveButton("확인",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int id) {
//								dialog.dismiss();
//							}
//						});
//
//		AlertDialog helpDialog = builder.create();
//		return helpDialog;
//	}
//
//
//	private class CounterServiceConnection implements ServiceConnection {
//		public void onServiceConnected(ComponentName className, IBinder boundService) {
//			counterService = ICounterService.Stub.asInterface((IBinder) boundService);
////			serviceStartButton.setText("STOP");//모니터링 종료
//			serviceStartButton.setBackgroundResource(R.drawable.spider_stop);
//			serviceStartButton.setEnabled(true);
//			preferenceButton.setEnabled(false);
//			end_icon.setEnabled(false);		//앱 선택할 수 없게 막아둠.
//
////			selectAppButton.setEnabled(false);
//		}
//
//		public void onServiceDisconnected(ComponentName className) {
//			counterService = null;
//			getApplicationContext().unbindService(conn);
//			getApplicationContext().bindService(serviceIntent, conn, 0);
//
//			Toast.makeText(UMLogger.this, "Monitoring STOP", Toast.LENGTH_SHORT).show();
////			serviceStartButton.setText("START");//모니터링 시작
//			serviceStartButton.setBackgroundResource(R.drawable.spider_start);
//			serviceStartButton.setEnabled(true);
//			preferenceButton.setEnabled(true);
//			end_icon.setEnabled(true);		//앱 선택할 수 있게 풀어둠.
//
////			selectAppButton.setEnabled(true);
//		}
//	}
//
//
//
//	// MediaScan Class which works on kitkat
//	private static class MediaScanFile extends MediaScannerConnection {
//		private static MediaScanFile mSingleton = null;
//
//		public static MediaScanFile getInstance(Context context, String path) {
//			if (mSingleton == null) {
//				mSingleton = new MediaScanFile(context, mScanClient);
//				mSingleton.connect();
//			}
//
//			return mSingleton;
//		}
//
//		public MediaScanFile(Context context, MediaScannerConnectionClient client) {
//			super(context, client);
//		}
//
//		private static MediaScannerConnectionClient mScanClient = new MediaScannerConnectionClient() {
//			@Override
//			public void onMediaScannerConnected() {
//				File file = new File(Environment.getExternalStorageDirectory() + "/Log/");
//				File[] fileNames = file.listFiles(new FilenameFilter() {
//					@Override
//					public boolean accept(File dir, String filename) {
//						return filename.endsWith(".txt");
//					}
//				});
//
//				if (fileNames != null) {
//					for (int i = 0; i < fileNames.length; i++) {
//						mSingleton.scanFile(fileNames[i].getAbsolutePath(), null);
//					}
//				}
//			}
//
//			@Override
//			public void onScanCompleted(String path, Uri uri) {
//				if (mSingleton != null) {
//					mSingleton.disconnect();
//					mSingleton = null;
//				}
//			}
//		};
//	}
//
//	/**
//	 * Represents an asynchronous login/registration task used to authenticate
//	 * the user.
//	 */
//	public class DeviceRePlaceTask extends AsyncTask<Map, Integer, String> {
//
//		private final String mImei;
//		private final String mDevice_name;
//
//		ProgressDialog progressDialog;
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			progressDialog = new ProgressDialog(UMLogger.this);
//			progressDialog.setTitle("잠시만 기다려주세요");
//			progressDialog.setMessage("디바이스를 등록 확인 중입니다.");
//			progressDialog.setCanceledOnTouchOutside(false);
//			progressDialog.show();
//		}
//
//		DeviceRePlaceTask(String imei, String device_name) {
//			mImei= imei;
//			mDevice_name = device_name;
//		}
//
//		@Override
//		protected String doInBackground(Map... maps) {
//			// TODO: attempt authentication against a network service.
//
//			try {
//				// Simulate network access.
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				return null;
//			}
//			// HTTP 요청 준비 작업
//			ServiceApiHttpClient.Builder http = new ServiceApiHttpClient.Builder("POST",
//					"http://"+ Global.getBaseUrl()+ ApiUrl.API_NUM_03);
//
//			// 파라미터를 전송한다.
//			http.addAllParameters(maps[0]);
////            http.addOrReplace("DATA","{"+maps[0]+"}");
//
//			// HTTP 요청 전송
//			ServiceApiHttpClient post = http.create();
//			post.request();
//
//			// 응답 상태코드 가져오기
//			int statusCode = post.getHttpStatusCode();
//
//			// 응답 본문 가져오기
//			String body = post.getBody();
//
//			return body;
//			// TODO: register the new account here.
//		}
//
//		String device_id;
//		String device_seq;
//		String have_device_seq;
//		String imei;
//		String ip_addr;
//		String port_no;
//		String os_ver;
//		String state;
//
//		@Override
//		protected void onPostExecute(String s) {
//			Log.d(TAG, "Device Info : \n" + s);
//
//
//			JSONObject jsonObject;
//			try{
//
//				if (!s.equals("") || !s.isEmpty()){
//					jsonObject = new JSONObject(s);
//					if(jsonObject != null && !jsonObject.equals("")){
//
//						device_id = jsonObject.getString("device_id") != null ? jsonObject.getString("device_id") : "";
//						device_seq = jsonObject.getString("device_seq") != null ? jsonObject.getString("device_seq") : "";
//						have_device_seq = jsonObject.getString("have_device_seq") != null ? jsonObject.getString("have_device_seq") : "";
//						com.pion.pro.eNDer.util.PreferenceManager.getInstance(mContext).setHaveDeviceSeq(have_device_seq);
//						imei = jsonObject.getString("imei") != null ? jsonObject.getString("imei") : "";
//						//ip_addr = jsonObject.getString("ip_addr") != null ? jsonObject.getString("ip_addr") : "";
//						//port_no = jsonObject.getString("port_no") != null ? jsonObject.getString("port_no") : "";
//						os_ver = jsonObject.getString("os_ver") != null ? jsonObject.getString("os_ver") : "";
//						state = jsonObject.getString("state") != null ? jsonObject.getString("state") : "";
//
//						if(state.equals("003")){
//							Toast.makeText(UMLogger.this, "등록되지 않는 제조사 디바이스 입니다. 아탐 서버에 제조사를 등록바랍니다.", Toast.LENGTH_SHORT).show();
//						}else if(state.equals("004")){
//							Toast.makeText(UMLogger.this, "등록할 수 없는 디바이스 입니다. 관리자에게 문의하여 주시기 바랍니다.", Toast.LENGTH_SHORT).show();
//						}else if(state.equals("001")){
//							Toast.makeText(UMLogger.this, "등록 확인되었습니다.", Toast.LENGTH_SHORT).show();
////
//						}else if(state.equals("101")){
//							Toast.makeText(UMLogger.this, "등록 되었습니다.", Toast.LENGTH_SHORT).show();
//
//						}
//
//					}else{
//						Toast.makeText(UMLogger.this, "디바이스 정보 가져오지 못함", Toast.LENGTH_SHORT).show();
//
//					}
//				}else{
//					Toast.makeText(UMLogger.this, "디바이스 정보 가져오지 못함", Toast.LENGTH_SHORT).show();
//
//				}
//
//			}catch (Exception e){
//				e.printStackTrace();
//			}
//
//			progressDialog.dismiss();
//		}
//
//		@Override
//		protected void onCancelled() {
//
//		}
//	}
//
//}
