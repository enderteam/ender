package com.pion.pro.eNDer.util;

/**
 * Created by Jungjoong.Kim on 2016. 9. 21..
 */
public class ApiUrl {

    /*
    * 기본 서버 URL
    * */
    public static final String BASE_URL = Global.getBaseUrl();

    /*
    * 공통 API Url
    * */
    public static final String API_GET_JSON_DATA = BASE_URL + "/api/getEnderData.do";

    /*G
    * api android & api server connect -> api_num_01
    * */
    public static final String API_NUM_01 = "/mainHome";        //introActivity

    /*
    * api android & api server LogIn Check -> api_num_02
    * */
    public static final String API_NUM_02 = "/loginCheck";      //LoginActivy

    /*
    * api android & api server 단말기 리스트 등록 Url -> api_num_03
    * */
    public static final String API_NUM_03 = "/deviceInsert";    //UMLogger(Main)

    /*
    * api android & api server 프로젝트 리스트 Url -> api_num_04
    * */
    public static final String API_NUM_04 = "/projectList";     //AppSelect

    /*
    * api android & api server log Data Insert Url -> api_num_05
    * */
    public static final String API_NUM_05 = "/logDataInsert";

    /*
    * api android & api server Mirror_exe_seq Select Url -> api_num_06
    * */
    public static final String API_NUM_06 = "/getExeDevice";

    /*
    * api android & api server Atam Ender Error Capture Image Data Insert Url -> api_num_06
    * */
    public static final String API_NUM_06_1 = "/atamEnderInsert";

    /*
    * api android & api server Setting Time Resource Insert Url -> api_num_07
    * */
    public static final String API_NUM_07 = "/resourceInsert";

    /*
    * api android & api server scene Capture Data Insert Url -> api_num_08
    * */
    public static final String API_NUM_08 = "/sceneInsert";

    /*
    * api android & api server Atam Ender Update (End) Url -> api_num_9
    * */
    public static final String API_NUM_09 = "/UpdateAtamEnder";

    /*
    * api android & api server AtamEnder Is Null Update (error)Url -> api_num_10
    * */
    public static final String API_NUM_10 = "/UpdateAtamEnderIsNull";

    /*
    * api android & api server Capture Image Object Data Insert Url -> api_num_11
    * */
    public static final String API_NUM_11 = "/objectData";


}
